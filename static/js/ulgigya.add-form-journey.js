(function (ulgigya) {

  ulgigya.AddFormJourney = klass({

    $consoleLog: null,
    $apiKey: null,
    $validateRespMsg: null,
    $dataCenterInput: null,
    $brandCodeInput: null,
    $marketSelect: null,
    $templateSelect: null,
    $qanda: null,

    initialize: function (options) {

      if (!String.prototype.includes) {
        String.prototype.includes = function (search, start) {
          'use strict';
          if (typeof start !== 'number') {
            start = 0;
          }

          if (start + search.length > this.length) {
            return false;
          } else {
            return this.indexOf(search, start) !== -1;
          }
        };
      }

      var _self = this;

      _self.$consoleLog = options.consoleLog;
      _self.$apiKey = options.apiKey;
      _self.$marketSelect = options.marketSelect;
      _self.$templateSelect = options.templateSelect;
      _self.$dataCenterInput = options.dataCenterInput;
      _self.$brandCodeInput = options.brandCodeInput;
      _self.$validateRespMsg = options.validateRespMsg;
      _self.$qanda = options.qanda;
      _self.addEventListeners(options);

    },
    addEventListeners: function (options) {

      var _self = this;

      $('input').one('blur keydown', function () {
        $(this).addClass('touched');

      }).on('blur', function () {
        if ($(this).val() === "") {
          $(this).addClass('empty');
        } else {
          $(this).removeClass('empty');
        }
      });

      $('#qandaModal').on('hidden.bs.modal', function () {
        _self.redrawQuestionsAndAnswers(options.qanda);
        //alert('The modal is now hidden. JSON:|' + JSON.stringify(options.qanda) + '|');
      });

      $('#apiKey').change(function (e) {
        _self.$validateRespMsg.empty();
        if ($(this)[0].checkValidity()) {
          $("#market").prop("disabled", false);
        } else {
          $('#market').prop('selectedIndex', 0);
          $("#market").prop("disabled", true);
        }
      });

      options.addFormJourneyForm.submit(function (e) {

        e.preventDefault();

        var formJourneys = [];
        var $form = options.addFormJourneyForm;
        var $formMessages = options.formMessages;
        var formArraysKeys = ["language"];
        var formData = ulgigya.util.getFormDataAsObject($form, formArraysKeys);
        delete formData.qanda;
        formData.qanda = options.qanda;
        $formMessages.parent().removeClass('d-none').addClass('d-block');
        $formMessages.append("<p> Adding Journey Forms.... <p>");

        _self.$consoleLog.pulse({backgroundColor: '#ffe4b5'},
            {duration: 3250, pulses: -1, interval: 800});

        $.ajax({
          type: 'POST',
          url: ulgigya.config.endpoints.addFormJourney.url(),
          headers: {
            "Content-Type": "application/json",
            "X-CSRF-Token": $('[name="_csrf"]').val()
          },
          data: JSON.stringify(formData)
        })
        .done(function (response) {

          // if session expired, ajax call will return login html and render inside div
          // this 'if' block avoids that, but is a BAD hack. Not sure how else to do it though.
          // See https://stackoverflow.com/questions/53543390/form-submission-using-ajax-in-spring-boot-how-do-i-redirect-to-login-page-when

          if (response.includes("<!DOCTYPE html>")) {
            window.location.replace("/login");
          } else {
            // Set the message text.
            $formMessages.append("<p>" + response + "</p>");
            _self.$consoleLog.pulse('destroy');
          }

        })
        .fail(function (data) {

          // Set the message text.
          if (data.responseText !== '') {
            $formMessages.append("<p>" + data.responseText + "</p>");
          } else {
            $formMessages.append(
                '<p> Oops! An error occurred and your message could not be sent. </p>');
          }
          _self.$consoleLog.pulse('destroy');
        });

      });
    },
    getBrandNameByAPIKey: function getBrandNameByAPIKey(item) {
      var _self = this;

      _self.$validateRespMsg.empty();

      $.ajax({
        type: 'GET',
        url: ulgigya.config.endpoints.brandNameByAPIKey.url(),
        dataType: 'JSON',
        data: {
          marketCode: _self.$marketSelect.val(),
          apiKey: _self.$apiKey.val()
        },
        success: function (data) {

          if (!data.profileStoreExists) {
            // Make sure that the validateRespMsg div has the 'error' class.
            _self.$validateRespMsg.removeClass('success').addClass(
                'error').append(
                "<p class='validateMessage'>" + data.ResponseMsg + "</p>");
          } else if (data.profileStoreExists) {
            // Market base Data Center & Brand code set in hidden variable
            _self.$dataCenterInput.val(data.dataCenter);
            _self.$brandCodeInput.val(data.brandCode);
            // Make sure that the validateRespMsg div has the 'success' class and set the message text
            _self.$validateRespMsg.removeClass('error').addClass(
                'success').append(
                "<p class='validateMessage'>" + data.ResponseMsg + "</p>");
          }
        },
        error: function (data) {
          //Need something here that isn't an alert
        }
      });
    },
    redrawQuestionsAndAnswers: function redrawQuestionsAndAnswers(qAndAs) {
      $('#questionAndAnswersList').empty();
        for(index = 0; index < qAndAs.length; index++) {
        var qA = qAndAs[index];
        var qandaBox = '' +
            '<div class="shadow p-3 mb-2 bg-white rounded">\n' +
                '<div class="d-flex justify-content-between bd-highlight">\n' +
                     '<div class="p-2 bd-highlight"> Question ' + (index+1) +'/' + qAndAs.length + '</div>\n' +
                     '<div class="p-2 bd-highlight">' + qA.answer.length +' possible answers</div>\n' +
                '</div>\n' +
                '<div class="d-flex justify-content-lg-center"><h3>' + qA.questionText + '</h3></div>\n' +
                '<div class="d-flex justify-content-between bd-highlight">\n' +
                    '<div class="p-2 bd-highlight">Category: ' + qA.categories + '</div>\n' +
                    '<div class="p-2 bd-highlight"><a href="#" onClick="addFormJourney.removeQandA(' + index + ')">Remove</a></div>\n' +
                '</div>\n' +
            '</div>';
        $('#questionAndAnswersList').append($(qandaBox));
      }
    },
    removeQandA : function removeQandA(index) {
      var _self = this;
      _self.$qanda.splice(index, 1);
      _self.redrawQuestionsAndAnswers(_self.$qanda);
    }

  });

})(ulgigya);





