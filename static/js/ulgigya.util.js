ulgigya.util = {

  getFormDataAsJson: function ($form, $arrays) {
    return JSON.stringify(this.getFormDataAsObject($form));
  },

  getFormDataAsObject: function ($form, $arrays) {
    var arr = $form.serializeArray();
    var obj = {};

    if ($arrays) {
      $arrays.forEach(function(key, index) {
        obj[key] = [];
      });
    }

    for (var i = 0; i < arr.length; i++) {
      if (arr[i].value) {
        if (obj[arr[i].name] === undefined) {
          obj[arr[i].name] = arr[i].value.trim();
        } else {
          if (!(obj[arr[i].name] instanceof Array)) {
            obj[arr[i].name] = [obj[arr[i].name]];
          }
          obj[arr[i].name].push(arr[i].value.trim());
        }
      }
    }
    return obj;
  },
  makeKeepAliveRequest: function () {
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
      xhr.setRequestHeader(header, token);
    });
  },
  hideConsoleLog: function ($element) {
    $element.next().empty();
    $element.parent().removeClass('d-block').addClass('d-none');
  }
};