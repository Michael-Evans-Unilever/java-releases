(function (ulgigya) {

    ulgigya.CreateProfileStore = klass({

        $brandNameSelect: null,
        $marketSelect: null,
        $dataCenterInput: null,
        $brandInput: null,
        $brandCodeInput: null,
        $consoleLog: null,
        $profileStoreResponses: [],
        createSandboxStore: false,

        initialize: function (options) {

            var _self = this;

            _self.$brandNameSelect = options.brandNameSelect;
            _self.$marketSelect = options.marketSelect;
            _self.$dataCenterInput = options.dataCenterInput;
            _self.$brandInput = options.brandInput;
            _self.$brandCodeInput = options.brandCodeInput;
            _self.$consoleLog = options.consoleLog;
            _self.createSandboxStore = options.createSandboxStore;
            _self.addEventListeners(options);
        },

        addEventListeners: function (options) {

            var _self = this;

            $('input').one('blur keydown', function () {
                $(this).addClass('touched');

            }).on('blur', function () {
                if ($(this).val() === "") {
                    $(this).addClass('empty');
                } else {
                    $(this).removeClass('empty');
                }
            });

            options.createProfileStoreForm.submit(function (e) {
                e.preventDefault();

                var $form = options.createProfileStoreForm;
                var $formMessages = options.formMessages;
                var $newsletterInputs = options.newsletterInput;
                var formArraysKeys = ["serviceIdNewsletters"];
                var formData = ulgigya.util.getFormDataAsObject($form,
                    formArraysKeys);

                $formMessages.parent().removeClass('d-none').addClass(
                    'd-block');

                $formMessages.append("<p> Creating Profile Store.... <p>");

                _self.$consoleLog.pulse({backgroundColor: '#ffe4b5'},
                    {duration: 3250, pulses: -1, interval: 800});

                $.when(
                    _self.postFormData(formData, $formMessages, 'UAT'),
                    _self.postFormData(formData, $formMessages, 'PROD')
                ).then(function() {
                    $formMessages.empty();
              });
            });
        },

        postFormData: function ($formData, $formMessages, $environment) {
            var _self = this;
            var myFormData = JSON.parse(JSON.stringify($formData));
            myFormData.environment = $environment;
            $.ajax({
                dataType: "json",
                type: 'POST',
                url: ulgigya.config.endpoints.createProfileStore.url(
                    _self.createSandboxStore),
                headers: {
                    "Content-Type": "application/json",
                    "X-CSRF-Token": $('[name="_csrf"]').val(),
                    "X-Username": "cppWebapp"
                },
                data: JSON.stringify(myFormData)
            })
            .done(function (response) {
                _self.$profileStoreResponses.push(response);
                _self.drawFormMessages($formMessages);
                // $formMessages.append("<p>" + response + "</p>");
                _self.$consoleLog.pulse('destroy');
            })
            .fail(function (jqxhr, exception) {
                $msg = "<div class=\"alert alert-warning\"><p><i class=\"fa fa-warning\"></i>";
                if (jqxhr.status === 0) {
                    $msg = $msg + 'Can not connect to server. Please check your network connection';
                } else if (jqxhr.status == 404) {
                    $msg = $msg + 'Requested page not found. <b>[Error - 404]</b>';
                } else if (jqxhr.status == 500) {
                    $msg = $msg + 'Internal Server Error <b>[Error - 500]</b>';
                } else if (exception === 'parsererror') {
                    $msg = $msg + 'Requested JSON parse failed';
                } else if (exception === 'timeout') {
                    $msg = $msg + 'Request Time out error';
                } else if (exception === 'abort') {
                    $msg = $msg + 'Request aborted';
                } else {
                    window.location.replace("/login");
                }

                // Set the message text.
                if (jqxhr.responseText !== '') {
                  var responseObject = JSON.parse(jqxhr.responseText);
                    $msg = $msg + "<p>" + $environment + ' - ' + responseObject.message + "</p>";
                } else {
                    $msg = $msg + '<p> Oops! An error occurred and your message could not be sent. </p>';
                }
                $msg = $msg + "</p></div>";
                $formMessages.append($msg);
                _self.$consoleLog.pulse('destroy');
            });
        },
        getBrandCode: function () {
            var _self = this;

            _self.$brandCodeInput.val(_self.$brandNameSelect.val());
            _self.$brandInput.val($('option:selected', _self.$brandNameSelect).text());
        },
        drawFormMessages: function ($formMessages) {
            $formMessages.empty();
            var _self = this;
            _self.$profileStoreResponses.sort(function (a, b) {
                return a.body.environment === 'SANDBOX' ? -1 : a.body.environment === 'UAT' ? 0 : 1;
            });
            for (i =0; i < _self.$profileStoreResponses.length; i++) {
                var element = _self.$profileStoreResponses[i];
                var panelClass = 'info';
                if (element.body.environment === 'PROD') {
                    panelClass = 'danger';
                }
                var $message = '<div class ="card border-' + panelClass + '">' +
                    '<div class="card-header bg-' + panelClass + '">' +
                    element.body.environment +
                    ' :: Creation Status :: <span class="titleCase">' + element.body.creationStatus + '</span></h3>' +
                    '</div><div class="card-body bg-transparent">' +
                    '  Key: ' + element.body.apiKey + '<br>' +
                    '<a href="#" class="aClipBoard" onClick=createProfileStore.getAPIKeyCopy(event,\'' + element.body.apiKey + '\')>Add a form journey to this profile store</a>' +
                    '</div></div><br/>';
                $formMessages.append($message);
            }
        },
        getMarketSelect: function getMarketSelect() {
            var _self = this;

            $.ajax({
                type: 'GET',
                url: ulgigya.config.endpoints.brandList.url(),
                dataType: 'JSON',
                data: {
                    marketCode: _self.$marketSelect.val()
                }
                }
            ).done(function (data) {
                var brandList = data.brandCodes;

                var stateSelect = _self.$brandNameSelect;
                stateSelect.find('option').remove();
                stateSelect.append(
                    '<option value="" hidden selected>Select your Brand</option>');
                // this loop to append Filtered Brand based on Market
                for (var i = 0; i < brandList.length; i++) {
                    stateSelect.append(
                        '<option value=' + brandList[i].brandCode + '>' +
                        brandList[i].brandName + '</option>');
                }
                // Based on Market selection , Data Center value will be store in Data Center Variable
                _self.$dataCenterInput.val(data.dataCenter);
            }).fail(function (data) {
                //Need something here that isn't an alert
            });

        },
        getAPIKeyCopy: function getAPIKeyCopy(event, fieldId) {
            event.preventDefault();

            var form = $('<form></form>');
            form.attr("method", "GET");
            form.attr("action", "/addFormJourney");
            form.data({"apiKey": fieldId});
            var field = $('<input></input>');
            field.attr("type", "hidden");
            field.attr("name", "apiKey");
            field.attr("value", fieldId);
            form.append(field);
            $(form).appendTo('body').submit();
        }

    });

})(ulgigya);