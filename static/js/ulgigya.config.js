ulgigya.config = {
    endpoints: {
        createProfileStore: {
            url: function (createSandboxStore) {
                if (createSandboxStore) {
                        return '/adminCreateSandboxSite';
                    }
                return '/createProfileStore';
            }
        },
        brandList: {
            url: function () {
               return '/getActiveBrandList';
            }
        },
        addFormJourney: {
             url: function () {
                 return '/addFormJourney';
             }
        },
        brandNameByAPIKey: {
             url: function () {
                 return '/getBrandNameByAPIKey';
             }
        },
        questionByCategoriesTxt: {
             url: function () {
                 return '/getAllQuestionsForCategory';
             }
        },
        answerByLanguage: {
             url: function () {
                 return '/getAllAnswersForLanguage';
             }
        }
    }
};