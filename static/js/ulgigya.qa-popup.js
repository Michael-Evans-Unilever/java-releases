(function (ulgigya) {

  'use strict';

  ulgigya.QAFormPopup = klass({

    $categorySelect: null,
    $answerSelect: null,
    $questionSelect: null,
    $answerInput: null,
    $qanda: null,
    initialize: function (options) {

      var _self = this;

      _self.$categorySelect = options.categorySelect;
      _self.$questionSelect = options.questionSelect;
      _self.$answerSelect = options.answerSelect;
      _self.$answerInput = options.answerInput;
      _self.$qanda = options.qanda;
      _self.$qaConsoleLog = options.qaConsoleLog;
      _self.$qaFormMessages = options.qaFormMessages;
      _self.addEventListeners(options);
    },

    addEventListeners: function (options) {

      var _self = this;
      options.addQAFormPage.submit(function (e) {

        e.preventDefault();

        var $form = options.addQAFormPage;
        var $formMessages = [];
        var $formData = ulgigya.util.getFormDataAsObject($form);
        $formData.questionText = $("#qaquestion option:selected").text();

        var vals = $('select#answer > option').map(function () {
          if (this.selected) {
            return this.value;
          }
        }).get();

        $formData.answer = vals;
        if (!$formData.categories) {
          $formMessages.push("Please select a category");
        }

        if (!$formData.qaquestion) {
          $formMessages.push("Please select a question");
        }

        if (vals.length < 2) {
          $formMessages.push("Please select at least 2 answers");
        }

        if ($formMessages.length === 0) {
          options.qanda.push($formData);
          $('#qandaModal').modal('hide');
          $('body').removeClass('modal-open');
          $('.modal-backdrop').remove();
        } else {
          var consoleLog =  _self.$qaConsoleLog;
          consoleLog.removeClass('d-none').addClass('d-block');
          $formMessages.forEach(function (message) {
            _self.$qaFormMessages.append("<p>" + message + "<p>");
          });
          consoleLog.pulse({backgroundColor : '#ffadba'}, { duration : 3250, pulses   : 2, interval : 800});
        }
      });

      $("#close-qa-console-log").click(function() {
        ulgigya.util.hideConsoleLog($(this));
      });

    },

    getCategorySelect: function getCategorySelect(values) {
      var _self = this;
      $.ajax({
            type: 'GET',
            url: ulgigya.config.endpoints.questionByCategoriesTxt.url(),
            dataType: 'JSON',
            data: {
              categoryText: _self.$categorySelect.val(),
              languageCode: 'en'
            }
          }
      ).done(function (data) {
        var questionSelect = _self.$questionSelect;
        questionSelect.find('option').remove();
        questionSelect.append(
            '<option value="" hidden selected>Select your Question</option>');
        // this loop to append question based on categories
        for (var i = 0; i < data.length; i++) {
          questionSelect.append(
              '<option value=' + data[i].questionId + '>' + data[i].questionText + '</option>');
        }
      }).fail(function (data) {
        //Need something here that isn't an alert
        console.log("Had an issue trying to get the questions", data);
      });
    },
    getQuestionSelect: function getQuestionSelect(values) {
      var _self = this;
      $.ajax({
            type: 'GET',
            url: ulgigya.config.endpoints.answerByLanguage.url(),
            dataType: 'JSON',
            data: {languageCode: 'en'}
          }
      ).done(function (data) {
        var answerSelect = _self.$answerSelect;
        answerSelect.find('option').remove();
        // answerSelect.append('<option value="" hidden selected>Select your Answer</option>');
        // this loop to append answer based on questions
        for (var i = 0; i < data.length; i++) {
          answerSelect.append(
              '<option value=' + data[i].answerId + '>' + data[i].answerText + '</option>');
        }
        answerSelect.data('plugin_lwMultiSelect').updateList();
      }).fail(function (data) {
        //Need something here that isn't an alert
      });
    }
  });

})(ulgigya);