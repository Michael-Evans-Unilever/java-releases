(function( window ) {

    'use strict';

    var doc = document,
        ulgigya = function(selector) {
            return jQuery(selector);
        };

    ulgigya.domReady = function (fn) {
        jQuery(doc).ready(fn);
    };

    window.ulgigya = ulgigya;
})( window );











