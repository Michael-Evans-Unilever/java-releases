
beforeEach(function() {
    jasmine.addMatchers({

        toContainThisHtml: function() {
            return {
                compare: function(actual, expected) {

                    if (expected === undefined) {
                        expected = '';
                    }
                    var result = {};

                    result.pass = actual.indexOf(expected) >= 0;
                    if (!result.pass) {
                        result.message = 'You Monkey! I expected '  + actual + ' to contain ' + expected;
                    }

                    return result;
                }
            }
        }
    });
});