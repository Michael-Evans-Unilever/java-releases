'use strict';

describe("ulgigya.create-profile-store", function() {

    var createProfileStore, clock, options;

    beforeEach(function() {
        jasmine.Ajax.install();
        clock = sinon.useFakeTimers();

        setFixtures(
            '<input type="hidden" data-brand-code name="brandCode" id="brandCode" />' +
            '<input type="hidden" data-brand name="brand" id="brand"/>' +
            '<input type="hidden" data-data-center name="dataCentre" id="dataCentre" />'+
            '<select data-brand-name-select id="brandName">' +
            '<option selected value="myBrandCode">myBrandName</option></select>'+
            '<select data-market-select>' +
            '<option value="de">Germany</option>'+
            '<option selected value="gb">United Kingdom</option></select>'
           );


        options = {
            createProfileStoreForm: $('#createProfileStore'),
            formMessages: $('#form-messages'),
            newsletterInput: $('#newsletterInput'),
            brandNameSelect: $('[data-brand-name-select]'),
            marketSelect: $('[data-market-select]'),
            dataCenterInput: $('[data-data-center]'),
            brandInput: $('[data-brand]'),
            brandCodeInput: $('[data-brand-code]')
        };

        createProfileStore = new ulgigya.CreateProfileStore(options);
    });

    afterEach(function() {
        jasmine.Ajax.uninstall();
        clock.restore();
        createProfileStore = null;
    });


    it('getBrandCode should populate hidden inputs correctly', function() {

        createProfileStore.getBrandCode();

        expect(options.brandInput.val()).toBe('myBrandName');
        expect(options.brandCodeInput.val()).toBe('myBrandCode');

    });

    it('getMarketSelect should populate brand drop down correctly', function() {
        var doneFn = jasmine.createSpy("success");

        var data = {
            brandCodes: [
                {brandName: 'brandname1', brandCode: 'brandcode1'},
                {brandName: 'brandname2', brandCode: 'brandcode2'},
                {brandName: 'brandname3', brandCode: 'brandcode3'}
            ],
            dataCenter: 'theDataCenter'
        };

        createProfileStore.getMarketSelect();

        var request = jasmine.Ajax.requests.mostRecent();
        request.respondWith({
            "status": 200,
            "responseText": data
        });

        var requestApiUrl = request.url.substring(0,request.url.indexOf('?'));
        expect(requestApiUrl).toEqual('/getActiveBrandList');
        expect(request.method).toBe('GET');
       // expect(doneFn).toHaveBeenCalledWith(data);


    });

});