'use strict';

describe("ulgigya.add-screenset", function() {

    var addFormJourney, clock, options;

    beforeEach(function() {
        jasmine.Ajax.install();
        clock = sinon.useFakeTimers();

        setFixtures(
            '<form id="addFormJourney">'+
            '<input type="hidden" data-brand-code name="brandCode" id="brandCode" value="brandcode1"/>' +
            '<input type="hidden" data-data-center name="dataCentre" id="dataCentre" value="dataCenter1"/>'+
            '<select data-market-select>' +
            '<option value="de">Germany</option>'+
            '<option selected value="gb">United Kingdom</option></select>'+
            '<input id="apiKey" type="text" name="siteApiKey" value="3_LYy_uDmY2xFHYDQsUvotRqjHIICpil0DLQpNeYzHEUyMyVrJ5AMXxa2njnPgFQW1">'+
            '<div class="validateMessage" id="validateRespMsg"></div>'+
            '</form>'
           );

        options = {
            addFormJourneyForm: $('#addFormJourney'),
            apiKey: $('#apiKey'),
            formMessages: $('#form-messages'),
            marketSelect: $('[data-market-select]'),
            dataCenterInput: $('[data-data-center]'),
            brandCodeInput: $('[data-brand-code]'),
            validateRespMsg : $('#validateRespMsg')
        };

        addFormJourney = new ulgigya.AddFormJourney(options);
    });

    afterEach(function() {
        jasmine.Ajax.uninstall();
        clock.restore();
        addFormJourney = null;
    });

    it('getBrandNameByAPIKey should populate brand code and data center correctly', function() {
        var doneFn = jasmine.createSpy("success");

        var data = {
             dataCenter: 'dataCenter1',
             brandCode: 'brandcode1',
             apiKey:'3_LYy_uDmY2xFHYDQsUvotRqjHIICpil0DLQpNeYzHEUyMyVrJ5AMXxa2njnPgFQW1',
             profileExists:'true'
        };

        addFormJourney.getBrandNameByAPIKey();

        var request = jasmine.Ajax.requests.mostRecent();
        request.respondWith({
            "status": 200,
            "responseText": data
        });

        var requestApiUrl = request.url.substring(0,request.url.indexOf('?'));
        expect(requestApiUrl).toEqual('/getBrandNameByAPIKey');
        expect(request.method).toBe('GET');
    });

});