'use strict';

describe("ulgigya.config", function() {

    it('create profile store url should be returned correctly for prod sites', function() {

        var result = ulgigya.config.endpoints.createProfileStore.url(true);

        expect(result).toBe('/createProfileStore');

    });
      it('create profile store url should be returned correctly for sandbox site', function() {

            var result = ulgigya.config.endpoints.createProfileStore.url(false);

            expect(result).toBe('/adminCreateSandboxSite');

        });
    it('brand list url should be returned correctly', function() {

        var result = ulgigya.config.endpoints.brandList.url();

        expect(result).toBe('/getActiveBrandList');

    });
    it('addFormJourney url should be returned correctly', function() {

        var result = ulgigya.config.endpoints.addScreenSet.url();

        expect(result).toBe('/addFormJourney');

    });
    it('BrandName By APIKey url should be returned correctly', function() {

            var result = ulgigya.config.endpoints.brandNameByAPIKey.url();

            expect(result).toBe('/getBrandNameByAPIKey');

    });

});