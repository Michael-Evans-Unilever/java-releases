'use strict';

describe("ulgigya.util", function() {

    beforeEach(function() {

        setFixtures(
            '<form id="addScreenSet">'+
            '<input type="hidden" data-brand-code name="brandCode" id="brandCode" value="brandcode1"/>' +
            '<input type="hidden" data-data-center name="dataCentre" id="dataCentre" value="dataCenter1"/>'+
            '<select data-market-select id="marketSelect" name="marketSelect">' +
            '<option value="de">Germany</option>'+
            '<option selected value="gb">United Kingdom</option></select>'+
            '<input id="apiKey" type="text" name="siteApiKey" value="mySiteAPIKey">'+
            '</form>'
           );
    });

    afterEach(function() {
    });

    it('getFormDataAsObject should populate form data', function() {
       var indexed_array= ulgigya.util.getFormDataAsObject($('#addScreenSet'));

        expect(indexed_array.brandCode).toEqual("brandcode1");
        expect(indexed_array.dataCentre).toEqual("dataCenter1");
        expect(indexed_array.marketSelect).toEqual("gb");
        expect(indexed_array.siteApiKey).toEqual("mySiteAPIKey");


    });
});