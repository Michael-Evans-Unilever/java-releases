module.exports = function (grunt) {

    var resourcesRoot = '../src/main/resources/';
    var publicRoot = resourcesRoot + '/static/public/';

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                separator: ';'
            },
            dist: {
                // options: {
                //     // Replace all 'use strict' statements in the code with a single one at the top
                //     banner: "'use strict';\n",
                //     process: function(src, filepath) {
                //         return '// Source: ' + filepath + '\n' +
                //             src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
                //     },
                // },
                src: [
                    publicRoot + 'js/libs/moment.js',
                    publicRoot + 'js/libs/jquery.pulse.js',
                    publicRoot + 'js/libs/klass.js',
                    publicRoot + 'js/libs/bootstrap-session-timeout.js',
                    'js/ulgigya.js',
                    'js/**/ulgigya.*.js',
                    '!js/libs/*.min.js'],
                dest: publicRoot + 'js/<%= pkg.name %>.js'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    '../src/main/resources/static/public/js/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
                }
            }
        },
        jshint: {
            files: ['Gruntfile.js', 'js/**/ulgigya*.js', 'test/**/*.js'],
            options: {           // options here to override JSHint defaults
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                },
                esversion: 6
            }
        },
        watch: {
            cssJs: {
                files: ['js/**/*.js', 'spec/**/*.js',
                    'templates-compiler/*.html'],
                tasks: ['concat', 'replace'],
                options: {
                    livereload: true
                },
                optimization: {
                    minimize: false
                }
            },
            files: ['<%= jshint.files %>'],
            tasks: ['jshint']
        },
        replace: {
            dist: {
                options: {
                    patterns: [
                        {
                            match: 'timestamp',
                            replacement: '<%= new Date().getTime() %>'
                        }
                    ]
                },
                files: [

                    {
                        src: ['templates-compiler/footer_js.html'],
                        dest: resourcesRoot + 'templates/footer_js.html'
                    }
                ]
            }
        },
        jasmine: {
            src: [
                publicRoot + 'js/unilevergigya.js'
            ],
            options: {
                specs: 'spec/*.spec.js',
                helpers: 'spec/helpers/*.js',
                vendor: [
                    publicRoot + 'js/libs/jquery-3.4.1.min.js',
                    publicRoot + 'js/libs/moment.js',
                    publicRoot + 'js/libs/klass.min.js',
                    publicRoot + 'js/libs/bootstrap-session-timeout.min.js'
                ]
            }
        },
        karma: {
            unit: {
                port: 9999,
                singleRun: false,
                browsers: ['Chrome'],
                logLevel: 'ERROR',
                options: {
                    frameworks: ['jasmine'],
                    files: [
                        publicRoot + 'js/libs/jquery-3.4.1.min.js',
                        publicRoot + 'js/libs/moment.js',
                        publicRoot + 'js/libs/klass.min.js',
                        publicRoot + 'js/libs/bootstrap-session-timeout.min.js',
                        publicRoot + 'js/unilevergigya.js',
                        'spec/*.spec.js',
                        'spec/helpers/*.js'
                    ]
                }
            }
        },
        standard: {
            app: {
                src: [
                    'js/ulgigya*.js'
                ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-replace');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-standard');

    grunt.registerTask('test', ['concat', 'uglify', ' replace', 'jasmine']);

    grunt.registerTask('dev', ['watch:cssJs']);

    grunt.registerTask('build', ['jshint', 'concat', 'uglify', 'replace']);

    grunt.registerTask('build-nolint', ['concat', 'uglify', 'replace']);

    grunt.registerTask('default', ['jshint', 'concat', 'uglify', 'replace']);

    grunt.registerTask('local', ['concat', 'replace']);

};