# Profile-Store-Webapp

####Requirements:
JDK 1.8 or later

####How To Run:

######Redis-Server 
To run the application locally with Redis enabled, Redis will need to be installed locally via Docker,
you can get this by typing `docker pull redis` (Docker must be installed) in the command line. To then start the server correctly
type `docker run --name=redis-server --publish=6379:6379 --hostname=redis --restart=on-failure --detach redis:latest`
This will start the server locally on port `6379`. The application's yml file will need to changed so that
`spring.redis.host` has the value of `localhost`. (See application.yml.example for exact configuration).

Note: If Redis functionality is not required, please refer to the application.yml.example file for changes required to 
disable redis functionality.

######From IDE

To start the application you will need to run the ProfileStoreWebappApplication class.
This will start up a tomcat server on port 8080.
In your preferred browser go to `http://localhost:8080/` to start using the application.

PLEASE NOTE: This will only use the application.yml file under src/main.resources which will contain default values
that will need replacing locally for the application to correctly work.

######From cmd

To build:
`mvn clean package`

At this point if you want to overwrite the default property values you will need to create a application.yml file
in the same directory as the .jar or inside of a /config directory. 
  
To run:
`java -jar $JarNameHere `
  
This should boot up a tomcat server and you can use your preferred browser to go to `http://localhost:8080/` and start
using the application.

####Spring-Boot Services:
This web service utilises the following spring-boot-starter dependencies:
<li> web </li>
<li> security </li>
<li> thymeleaf </li>
<li> test </li>
<br>
These should atomatically be pulled in via your IDE and need no configuration to work as intended.

