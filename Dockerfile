FROM openjdk:8-jdk-alpine
ARG BUILD_NUMBER=0
VOLUME /tmp
EXPOSE 8080
EXPOSE 8443
COPY ./target/*.jar /profile-store-webapp.jar
COPY application.yml /application.yml

ENTRYPOINT ["java","-Dspring.profiles.active=ENVIRONMENT","-Djava.security.egd=file:/dev/./urandom","-jar","/profile-store-webapp.jar"]
