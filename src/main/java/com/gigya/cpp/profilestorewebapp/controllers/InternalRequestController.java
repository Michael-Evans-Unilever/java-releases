package com.gigya.cpp.profilestorewebapp.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.gigya.cpp.generated.profilestorewebapp.model.ProfileRequest;
import com.gigya.cpp.generated.profilestorewebapp.model.ProfileStoreResponse;
import com.gigya.cpp.profilestorewebapp.model.pojos.Answer;
import com.gigya.cpp.profilestorewebapp.model.pojos.Answers;
import com.gigya.cpp.profilestorewebapp.model.pojos.Categories;
import com.gigya.cpp.profilestorewebapp.model.pojos.FormJourney;
import com.gigya.cpp.profilestorewebapp.model.pojos.Question;
import com.gigya.cpp.profilestorewebapp.model.requests.AddFormJourneyRequest;
import com.gigya.cpp.profilestorewebapp.service.CsvReaderService;
import com.gigya.cpp.profilestorewebapp.service.ProfileStoreService;
import com.gigya.cpp.profilestorewebapp.service.QuestionAnswerService;
import com.gigya.cpp.profilestorewebapp.service.orchestrators.FormJourneyOrchestrator;
import com.gigya.cpp.profilestorewebapp.util.exceptions.StatusCodeNot2xxException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class InternalRequestController {

    private final FormJourneyOrchestrator formJourneyOrchestrator;
    private final QuestionAnswerService questionAnswerService;
    private final CsvReaderService csvReaderService;
    private final ProfileStoreService profileStoreService;

    @Autowired
    public InternalRequestController(ProfileStoreService profileStoreService,
        FormJourneyOrchestrator formJourneyOrchestrator,
        QuestionAnswerService questionAnswerService,
        CsvReaderService csvReaderService) {
        this.formJourneyOrchestrator = formJourneyOrchestrator;
        this.questionAnswerService = questionAnswerService;
        this.csvReaderService = csvReaderService;
        this.profileStoreService = profileStoreService;
    }

    @PostMapping("/addFormJourney")
    @CrossOrigin
    public String addFormJourney(@RequestBody AddFormJourneyRequest addFormJourneyRequest) {
        log.debug("::Enter into add Form Journey");
        //Gets the first language from the list
        List<String> formLanguage = addFormJourneyRequest.getLanguage();

        String firstLanguage = null;
        if (!formLanguage.isEmpty()) {
            firstLanguage = formLanguage.get(0);
        }

        // Retrieves the parent api key
        String parentApiKey;
        try {
            parentApiKey = profileStoreService.getParentApiKey(addFormJourneyRequest.getSiteApiKey(), addFormJourneyRequest.getDataCentre());

        } catch (StatusCodeNot2xxException e) {
            log.error("Response from profile-store-information non-200", e);
            return "Forms not added - contact support team";
        } catch (Exception e) {
            log.error("Unable to read response from profile-store-information service", e);
            return "Forms not added - contact support team";
        }

        if (parentApiKey.equals("doesn't exist")) {
            return "API Key doesn't exist.";
        }

        // Sets language and other mappable fields from the request
        FormJourney formJourneyDTO = new FormJourney();
        formJourneyDTO.setLanguage(firstLanguage);
        formJourneyDTO.setApiKey(parentApiKey);
        formJourneyDTO.setScreenset(addFormJourneyRequest.getTemplateSelect());
        BeanUtils.copyProperties(addFormJourneyRequest, formJourneyDTO, "language");

        // Sends request for the selected Journey
        try {
            formJourneyOrchestrator.addFormJourney(formJourneyDTO);
        } catch (StatusCodeNot2xxException e) {
            log.error("StatusCodeNot2xxException! Status Code: {}. Status msg: {} ", e.getStatusCode().value(), e.getStatusText());
            log.warn("Error setting {} on {}", addFormJourneyRequest.getTemplateSelect(), parentApiKey);
            log.debug("Exit from add form journey");
            return "Selected Form Journey is not applicable to the Market selected.";
        } catch (Exception e) {
            log.error("Unable to read response from profile-store-information service", e);
            log.debug("Exit from add form journey");
            return "Forms not added - contact support team";
        }

        // Builds the response
        String formJourney = addFormJourneyRequest.getFormJourney();
        StringBuilder outPutMsg = new StringBuilder();
        outPutMsg.append("All forms added successfully!</br>");
        outPutMsg.append("<b>ALL FORMS : </b></br>");
        outPutMsg.append("Add the following script code into your web page's head section :");
        outPutMsg.append("<xmp id=\"allForms\">");
        outPutMsg.append("<script type=\"text/javascript\" src=\"https://cdns.gigya.com/JS/gigya.js?");
        outPutMsg
            .append("apiKey=" + addFormJourneyRequest.getSiteApiKey() + "&lang=" + firstLanguage + "-" + addFormJourneyRequest.getMarket() + "\"></script>");
        outPutMsg.append("\n<script src=\"//cdn.gigya-ext.com/gy.js\" type=\"text/javascript\"></script> </xmp>" +
            "<a href=\"#\" class=\"aClipBoard\" onClick=getCopyText(event,\"allForms\")>Copy to clipboard</a></br>");

        outPutMsg.append("</br><p><b>FORM : " + addFormJourneyRequest.getFormJourney() + "</b>:</br>");
        outPutMsg.append("Add the following html code into your webpage body section form : </p>");
        outPutMsg.append("<xmp id=\"" + formJourney + "\"><div class=\"gy-ui-screen-set\" data-screen-set=\"" + formJourney + "\"></xmp>" +
            "<a href=\"#\" class=\"aClipBoard\" onClick=getCopyText(event,\"" + formJourney + "\")>Copy to clipboard</a></br>");

        log.debug("Exit from add form journey");
        return outPutMsg.toString();
    }

    @GetMapping("/getBrandNameByAPIKey")
    @CrossOrigin
    public @ResponseBody
    Map<String, Object> getBrandNameByAPIKey(@RequestParam(name = "marketCode", required = true, defaultValue = "gb") String marketCode,
        @RequestParam(name = "apiKey", required = true) String apiKey, Model model) throws FileNotFoundException {
        log.debug("::Enter into getBrandNameByAPIKey");
        Map<String, Object> displayData = new HashMap<>();
        try {
            String dataCenter = csvReaderService.getDataCenterMarkets(marketCode);
            JsonNode root = profileStoreService.getBrandMarketByApiKey(apiKey, dataCenter);
            if (root.get("siteExists") == null || !root.get("siteExists").asBoolean()) {
                displayData.put("Sucess", true);
                displayData.put("profileStoreExists", false);
                displayData.put("ResponseMsg", "API Key doesn't exist.");
            } else {
                displayData.put("Success", true);
                displayData.put("profileStoreExists", true);
                displayData.put("brandCode", root.get("brandCode").textValue());
                displayData.put("dataCenter", dataCenter);
                displayData.put("ResponseMsg", "API Key validated successfully!");
            }

        } catch (StatusCodeNot2xxException e) {
            log.error("Response from profile-store-information non-200.", e);
            displayData.put("ResponseMsg", "Error in validating API key  - contact support team");
        } catch (HttpMessageNotReadableException e) {
            log.error("Unable to read response from profile-store-information service.", e);
            displayData.put("ResponseMsg", "Error in validating API key  - contact support team");
        }
        log.debug("Exit from getBrandNameByAPIKey");
        return displayData;
    }

    @PostMapping("/createProfileStore")
    @CrossOrigin
    public ProfileStoreResponse createProfileStore(@RequestBody ProfileRequest profileRequest) {
        log.debug("::Enter into Create Profile Store");
        ProfileStoreResponse msg = profileStoreService.createProfileStore(profileRequest);
        log.debug("Exit from Create profile Store");
        return msg;
    }


    @PostMapping("/adminCreateSandboxSite")
    @CrossOrigin
    public ProfileStoreResponse createSandboxSite(@RequestBody ProfileRequest profileRequest) {
        log.debug("::Enter into Create Admin Profile Store");
        ProfileStoreResponse msg = profileStoreService.createProfileStore(profileRequest);
        log.debug("Exit from Create profile Store");
        return msg;
    }

    @GetMapping("/getAllQuestionCategories")
    @CrossOrigin
    public @ResponseBody
    List<String> getAllQuestionCategories() throws IOException {

        log.debug("::Enter into getList All QuestionOa3 Categories");
        Categories categoriesList = questionAnswerService.getAllQuestionCategories();
        log.debug("Exit from getList All QuestionOa3 Categories");

        return categoriesList.getCategories();
    }

    @GetMapping("/getAllQuestionsForCategory")
    @CrossOrigin
    public @ResponseBody
    List<Question> getAllQuestionsForCategory(@RequestParam(name = "categoryText", required = true) String categoryText,
        @RequestParam(name = "languageCode", required = true) String languageCode, Model model) throws FileNotFoundException {
        log.debug("::Enter into getList All QuestionOa3 For Categories");
        List<Question> questionList = questionAnswerService.getAllQuestionForCategory(categoryText, languageCode);
        log.debug("Exit from getList All QuestionOa3 For Categories");

        return questionList;
    }

    @GetMapping("/getAllAnswersForLanguage")
    @CrossOrigin
    public @ResponseBody
    List<Answer> getAllAnswersForLanguage(@RequestParam(name = "languageCode", required = true) String languageCode, Model model)
        throws FileNotFoundException {
        log.debug("::Enter into getList All Answers For Language");
        Answers answers = questionAnswerService.getAllAnswersForLanguage(languageCode);
        log.debug("Exit from getList All Answers For Language");

        return answers.getAnswers();
    }

    @GetMapping("/keep-alive")
    @CrossOrigin
    public @ResponseBody
    Response keepAlive() throws FileNotFoundException {

        log.debug("::keeping session alive");

        return Response.ok().build();
    }
}
