package com.gigya.cpp.profilestorewebapp.controllers;

import com.gigya.cpp.profilestorewebapp.config.OidcAuthentication;
import com.gigya.cpp.profilestorewebapp.config.UrlPropertyConfiguration;
import com.gigya.cpp.profilestorewebapp.model.pojos.AwsSecretKeys;
import com.gigya.cpp.profilestorewebapp.model.pojos.SsoResponse;
import com.gigya.cpp.profilestorewebapp.service.AuthenticationService;
import com.gigya.cpp.profilestorewebapp.service.SecretsHandler;
import java.net.URI;
import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Controller
@Slf4j
@RequiredArgsConstructor
public class SsoLoginController {

    private final RestTemplate restTemplate;
    private final SecretsHandler secretsHandler;
    private final UrlPropertyConfiguration urlPropertyConfiguration;
    private final AuthenticationService authenticationService;

    @GetMapping("/login")
    public String showLogin(HttpSession session, HttpServletRequest request, Model model, String error, String ssoError, String logout) {
        // if already logged in, redirect to homepage
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            return "redirect:/myProfileStore";
        }

        if (error != null) {
            model.addAttribute("error", "Incorrect credentials");
        }

        if (ssoError != null) {
            model.addAttribute("ssoError", "You have not been authorised to use this application");
        }

        if (logout != null) {
            model.addAttribute("logout", "Logged Out");
        }

        return "login";
    }

    @GetMapping("/ssoLogin")
    public String ssoLogin(HttpServletRequest request, HttpSession session) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            return "redirect:/myProfileStore";
        }

        //Assigns a new session during login process to protect against session attacks
        session.invalidate();
        request.getSession();
        AwsSecretKeys awsSecretKeys = secretsHandler.getSecretsFromAws();
        URI ssoHostUri = URI.create(urlPropertyConfiguration.getAzureSsoHost());

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("client_id", awsSecretKeys.getClientID());
        params.add("response_type", "code");
        params.add("redirect_uri", urlPropertyConfiguration.getAzureSsoRedirectUri());
        params.add("response_mode", "form_post");
        params.add("scope", "openid");
        params.add("state", awsSecretKeys.getState());
        UriComponentsBuilder authoriseUri = UriComponentsBuilder.fromUri(ssoHostUri).path(awsSecretKeys.getTenant() + "/oauth2/v2.0/authorize")
            .queryParams(params);
        return "redirect:" + authoriseUri.toUriString();
    }

    @PostMapping("/ssoLogin")
    public String validateToken(@RequestParam(name = "code", required = false) String idToken,
        @RequestParam(name = "state", required = false) String state,
        @RequestParam(name = "error", required = false) String error,
        @RequestParam(name = "error_description", required = false) String errorDescription) throws AuthenticationException {
        AwsSecretKeys awsSecretKeys = secretsHandler.getSecretsFromAws();

        if (error != null || !state.equals(awsSecretKeys.getState())) {
            log.error("Unable to sign a user in via Azure AD login");
            log.error(error);
            log.error(errorDescription);
            return "redirect:/login?ssoError";

        }

        URI ssoHostUri = URI.create(urlPropertyConfiguration.getAzureSsoHost());
        OidcAuthentication oidcAuthentication = new OidcAuthentication();

        oidcAuthentication.setAccessToken(idToken);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("client_id", awsSecretKeys.getClientID());
        map.add("scope", "openid");
        map.add("code", idToken);
        map.add("redirect_uri", urlPropertyConfiguration.getAzureSsoRedirectUri());
        map.add("grant_type", "authorization_code");
        map.add("client_secret", awsSecretKeys.getClientSecret());

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        UriComponentsBuilder tokenUri = UriComponentsBuilder.fromUri(ssoHostUri).path(awsSecretKeys.getTenant() + "/oauth2/v2.0/token");

        ResponseEntity<SsoResponse> response = restTemplate.postForEntity(tokenUri.toUriString(), request, SsoResponse.class);
        if (response.getStatusCodeValue() < 300) {
            String accessToken = response.getBody().getAccessToken();
            if (accessToken != null && !accessToken.equals("")) {
                oidcAuthentication.setAccessToken(accessToken);
                authenticationService.authenticate(oidcAuthentication);
                SecurityContext securityContext = SecurityContextHolder.getContext();
                securityContext.setAuthentication(oidcAuthentication);
            } else {
                throw new BadCredentialsException("Unable to authenticate with the Access Token Server");
            }
        } else {
            throw new HttpClientErrorException(HttpStatus.FORBIDDEN, "Unauthorised login, please try again");
        }
        return "redirect:/myProfileStore";
    }
}
