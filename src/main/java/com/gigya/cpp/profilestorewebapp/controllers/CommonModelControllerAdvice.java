package com.gigya.cpp.profilestorewebapp.controllers;

import com.gigya.cpp.profilestorewebapp.model.pojos.WebAppEnvironments;
import io.micrometer.core.instrument.util.StringUtils;
import javax.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class CommonModelControllerAdvice {

    private final Environment environment;

    @ModelAttribute
    public void setGlobalModelAttributes(HttpServletRequest request, Model model) {
        if (environment.getActiveProfiles() != null && environment.getActiveProfiles().length > 0) {
            log.info("Searching for active profiles {}", environment.getActiveProfiles());
            boolean found = false;
            for (String env : environment.getActiveProfiles()) {
                WebAppEnvironments webAppEnvironments = WebAppEnvironments.fromSpringProfile(env);
                if (webAppEnvironments != WebAppEnvironments.UNKNOWN) {
                    setModelAttributes(model, webAppEnvironments);
                    found = true;
                    break;
                }
            }
            if (!found) {
               log.error("Unable to find WebAppEnvironment for active profiles of '{}'",  environment.getActiveProfiles());
               setModelAttributes(model, WebAppEnvironments.UNKNOWN);
            }
        } else {
            log.error("The does not appear to be any active profiles set!");
        }
    }

    private void setModelAttributes(Model model, WebAppEnvironments webAppEnvironments) {
        if (StringUtils.isNotBlank(webAppEnvironments.getFriendlyName())) {
            model.addAttribute("watermarkName", webAppEnvironments.getFriendlyName());
            model.addAttribute("watermarkColour", webAppEnvironments.getWatermarkColour());
            model.addAttribute("watermarkWidth", webAppEnvironments.getWidth());
        }
        model.addAttribute("usePackedJs", webAppEnvironments.isUsePackedJs());
    }
}
