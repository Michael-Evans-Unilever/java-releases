package com.gigya.cpp.profilestorewebapp.controllers;

import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Slf4j
public class CustomErrorController implements ErrorController {

    @RequestMapping("/error")
    public String handleError(HttpServletRequest request, Model model) {
        log.debug("::Enter into Error handle Controller");
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        Exception exception = (Exception) request.getAttribute("javax.servlet.error.exception");
        model.addAttribute("status", statusCode);
        if (null != exception) {
            model.addAttribute("error", exception.getMessage());
        } else {
            model.addAttribute("error", "N/A");
        }
        log.debug("Exit from Custom Error Controller");
        return "error";

    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
