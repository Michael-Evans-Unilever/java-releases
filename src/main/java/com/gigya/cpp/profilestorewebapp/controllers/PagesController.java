package com.gigya.cpp.profilestorewebapp.controllers;


import com.gigya.cpp.profilestorewebapp.model.mappings.BrandCodeMapping;
import com.gigya.cpp.profilestorewebapp.model.mappings.LanguageMapping;
import com.gigya.cpp.profilestorewebapp.model.mappings.MarketMapping;
import com.gigya.cpp.profilestorewebapp.model.pojos.Answer;
import com.gigya.cpp.profilestorewebapp.model.pojos.Categories;
import com.gigya.cpp.profilestorewebapp.model.pojos.Question;
import com.gigya.cpp.profilestorewebapp.service.CsvReaderService;
import com.gigya.cpp.profilestorewebapp.service.QuestionAnswerService;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;

@Controller
@Slf4j
public class PagesController {

    private static final String FILE_NOT_FOUND_ERROR = "FILE NOT FOUND";
    private static final String BRAND_CODES_STRING = "brandCodes";
    private static final String MARKETS_STRING = "markets";
    private static final String LANGUAGES_STRING = "languages";
    private static final String DATA_CENTRE_STRING = "dataCenter";
    private static final String CATEGORIES_STRING = "categories";
    private static final String QUESTIONS_STRING = "questions";
    private static final String ANSWERS_STRING = "answers";

    private final CsvReaderService csvReaderService;

    private final boolean showTestScreensets;

    private final QuestionAnswerService questionAnswerService;

    @Autowired
    public PagesController(CsvReaderService csvReaderService,
        QuestionAnswerService questionAnswerService,
        @Value("${featureSwitches.showTestScreensets:false}") boolean showTestScreensets) {
        this.csvReaderService = csvReaderService;
        this.showTestScreensets = showTestScreensets;
        this.questionAnswerService = questionAnswerService;
    }

    @GetMapping("/createProfileStorePage")
    public String displayCreateProfileStorePage(@RequestParam(name = "error", required = false) String error, Model model) {
        log.debug("::Enter into display Create Profile Store Page ");
        if (error != null) {
            //should redirect to a real error page
            return "createProfileStore";
        }

        try {
            List<BrandCodeMapping> brandCodes = new LinkedList<>();
            List<MarketMapping> markets = csvReaderService.getAllMarkets();
            List<LanguageMapping> languages = csvReaderService.getAllLanguage();
            model.addAttribute(BRAND_CODES_STRING, brandCodes);
            model.addAttribute(MARKETS_STRING, markets);
            model.addAttribute(LANGUAGES_STRING, languages);
        } catch (FileNotFoundException e) {
            log.error(FILE_NOT_FOUND_ERROR, e);
            return "redirect:/createProfileStorePage?error=500";
        }
        log.debug("Exit from display Create Profile Store Page ");
        return "createProfileStore";
    }

    @GetMapping("/myProfileStore")
    public String displayProfilesPage() {
        return "profileStore";
    }

    @GetMapping("/addFormJourney")
    public String displayAddFormJourneyPage(@RequestParam(name = "error", required = false) String error,
        @RequestParam(name = "apiKey", required = false) String apiKey, Model model) {
        log.info("::Enter into display add Form Journey page");
        if (error != null) {
            //should redirect to a real error page
            return "addFormJourneyPage";
        }
        try {
            List<BrandCodeMapping> brandCodes = csvReaderService.getAllBrands();
            List<MarketMapping> markets = csvReaderService.getAllMarkets();
            List<LanguageMapping> languages = csvReaderService.getAllLanguage();
            model.addAttribute(BRAND_CODES_STRING, brandCodes);
            model.addAttribute(MARKETS_STRING, markets);
            model.addAttribute(LANGUAGES_STRING, languages);
            model.addAttribute("showTestScreensets", showTestScreensets);

            Categories categoriesList = questionAnswerService.getAllQuestionCategories();
            List<Question> questionList = new LinkedList<>();
            List<Answer> answerList = new LinkedList<>();
            model.addAttribute(LANGUAGES_STRING, languages);
            model.addAttribute(CATEGORIES_STRING, categoriesList.getCategories());
            model.addAttribute(QUESTIONS_STRING, questionList);
            model.addAttribute(ANSWERS_STRING, answerList);

            if (null != apiKey) {
                model.addAttribute("apiKey", apiKey);
            } else {
                model.addAttribute("apiKey", "");
            }
        } catch (IOException e) {
            log.error(FILE_NOT_FOUND_ERROR, e);
            return "redirect:/addFormJourney?error=500";
        }

        log.debug("Exit from display add Form Journey Page ");
        return "addFormJourneyPage";
    }

    @GetMapping("/getActiveBrandList")
    public @ResponseBody
    Map<String, Object> activeBrandList(@RequestParam(name = "marketCode", required = true, defaultValue = "gb") String marketCode, Model model) {
        log.debug("::Enter into getList active Brand List for marketCode: {}", marketCode);
        try {
            Map<String, Object> displayData = new HashMap<>();
            Set<BrandCodeMapping> filteredBrandList = csvReaderService.getAllMarketBrand(marketCode).stream()
                .collect(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(BrandCodeMapping::getBrandName))));
            String dataCenter = csvReaderService.getDataCenterMarkets(marketCode);
            displayData.put(BRAND_CODES_STRING, filteredBrandList);
            displayData.put(DATA_CENTRE_STRING, dataCenter);
            log.debug("Exit from getList active Brand List ");
            return displayData;
        } catch (FileNotFoundException e) {
            log.error(FILE_NOT_FOUND_ERROR, e);
            return null;
        }
    }

    @GetMapping("/addQAForm")
    public String displayQAForm(@RequestParam(name = "error", required = false) String error, Model model) {
        log.info("Session Id::" + RequestContextHolder.currentRequestAttributes().getSessionId() + "::Enter into display QA Form");
        if (error != null) {
            //should redirect to a real error page
            return "addQAFormPage";
        }
        try {

            List<LanguageMapping> languages = csvReaderService.getAllLanguage();
            Categories categoriesList = questionAnswerService.getAllQuestionCategories();
            List<Question> questionList = new LinkedList<>();
            List<Answer> answerList = new LinkedList<>();
            model.addAttribute(LANGUAGES_STRING, languages);
            model.addAttribute(CATEGORIES_STRING, categoriesList);
            model.addAttribute(QUESTIONS_STRING, questionList);
            model.addAttribute(ANSWERS_STRING, answerList);

        } catch (Exception e) {
            log.error(FILE_NOT_FOUND_ERROR, e);
            return "redirect:/addQAForm?error=500";
        }
        log.info("Exit from display add Form Journey Page ");
        return "addQAFormPage";
    }

    @GetMapping("/deleteSitePage")
    public String displayDeleteSitePage() {
        return "deletesitespage";
    }

    @GetMapping("/createSandboxSite")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String displayCreateSandboxSite() {
        return "createSandboxProfileStore";
    }

    @GetMapping("/addChildSite")
    public String displayAddChildSitePage() {
        return "addchildsite";
    }

}

