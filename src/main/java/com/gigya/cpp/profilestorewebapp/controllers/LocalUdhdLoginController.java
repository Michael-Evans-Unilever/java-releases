package com.gigya.cpp.profilestorewebapp.controllers;

import com.gigya.cpp.profilestorewebapp.config.CustomAuthentication;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@Profile("local")
@Slf4j
public class LocalUdhdLoginController {

    @PostMapping("/login")
    public String udhdLogin(HttpSession session, HttpServletRequest request,
        @RequestParam(name = "username", required = true) String username,
        @RequestParam(name = "password", required = true) String password) {
        session.invalidate();
        request.getSession();
        if (username.contains("@cpp.test")) {
            log.debug("::LOCAL Authentication Success For User ::" + username);
            CustomAuthentication customAuthentication = new CustomAuthentication();
            customAuthentication.setAuthenticated(true);
            customAuthentication.setUsername(username);
            SecurityContextHolder.getContext().setAuthentication(customAuthentication);
            return "redirect:/myProfileStore";
        }
        log.error("::Authentication Failure For User");
        return "redirect:/login?error";
    }
}
