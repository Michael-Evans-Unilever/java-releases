package com.gigya.cpp.profilestorewebapp.controllers;

import com.gigya.cpp.profilestorewebapp.config.CustomAuthentication;
import com.gigya.cpp.profilestorewebapp.config.UrlPropertyConfiguration;
import com.gigya.cpp.profilestorewebapp.service.AuthenticationService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

@Controller
@Profile("!local")
@RequiredArgsConstructor
@Slf4j
public class UdhdLoginController {

    private final RestTemplate restTemplate;
    private final AuthenticationService authenticationService;
    private final UrlPropertyConfiguration urlPropertyConfiguration;

    @PostMapping("/login")
    public String udhdLogin(HttpSession session, HttpServletRequest request,
        @RequestParam(name = "username", required = true) String username,
        @RequestParam(name = "password", required = true) String password) {

        session.invalidate();
        request.getSession();
        String localUrl = urlPropertyConfiguration.getUdhdUrl().replace("username", username);
        String fullUrl = localUrl.replace("password", password);

        ResponseEntity response = restTemplate.getForEntity(fullUrl, String.class);

        if (!(response.getStatusCode().is2xxSuccessful())) {
            log.info("::Could not connect to the UDHD authentication service :: {}", response.getStatusCode());
            throw new InternalError("Could not receive authentication");
        }

        if (response.getBody().toString().equals("true")) {
            CustomAuthentication customAuthentication = new CustomAuthentication();
            customAuthentication.setAuthenticated(true);
            customAuthentication.setUsername(username);
            authenticationService.authenticate(customAuthentication);
            log.debug("::Authentication Success For User::{}", username);

            SecurityContextHolder.getContext().setAuthentication(customAuthentication);
            return "redirect:/myProfileStore";
        }
        log.warn("::Authentication Failure For User::{}", username);
        return "redirect:/login?error";
    }
}
