package com.gigya.cpp.profilestorewebapp.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigya.cpp.generated.profilestorewebapp.model.ProfileRequest;
import com.gigya.cpp.generated.profilestorewebapp.model.ProfileStoreResponse;
import com.gigya.cpp.profilestorewebapp.config.KeysPropertyConfiguration;
import com.gigya.cpp.profilestorewebapp.config.UrlPropertyConfiguration;
import com.gigya.cpp.profilestorewebapp.util.CppRestTemplate;
import com.gigya.cpp.profilestorewebapp.util.exceptions.StatusCodeNot2xxException;
import java.io.IOException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@Slf4j
@Getter
@RequiredArgsConstructor
public class ProfileStoreService {

    private final CppRestTemplate restTemplate;
    private final SecretsHandler secretsHandler;
    private final UrlPropertyConfiguration urls;
    private final KeysPropertyConfiguration keys;


    public ProfileStoreResponse createProfileStore(ProfileRequest profileRequest) {
        log.debug("::Enter into createProfileStore");
        String userId = (String) RequestContextHolder.currentRequestAttributes().getAttribute("X-Username", RequestAttributes.SCOPE_REQUEST);
        HttpEntity<ProfileRequest> entity = restTemplate
            .getHttpEntity(profileRequest, secretsHandler.getSecretsFromAws().getProfileStoreUsername(), secretsHandler.getSecretsFromAws().getProfileStorePassword(), userId);
        ResponseEntity<ProfileStoreResponse> response = restTemplate
            .postUsingServiceNameWithHttpEntity(entity, "createProfileStore", ProfileStoreResponse.class);

        log.debug("Exit from createProfileStore");
        return response.getBody();
    }

    public String getParentApiKey(String childApiKey, String dataCentre) {
        log.debug("::Enter into getList Parent ApiKey");
        UriComponentsBuilder builder =
            UriComponentsBuilder.fromHttpUrl(urls.getGetParentApiKey())
                .queryParam("apiKey", childApiKey)
                .queryParam("dataCentre", dataCentre);

        HttpHeaders headers = new HttpHeaders();
        headers.set("x-api-key", keys.getKey("sandboxApiKey"));
        HttpEntity<?> entity = new HttpEntity<>(headers);

        ResponseEntity response;

        try {
            response = restTemplate.getRequest(entity, builder.toUriString());
        } catch (StatusCodeNot2xxException e) {
            log.info("Non-200 returned by Gigya! Status code received: ", e);
            throw new StatusCodeNot2xxException(e.getStatusCode(), e.getStatusText());
        }

        ObjectMapper mapper = new ObjectMapper();
        JsonNode root;
        try {
            root = mapper.readTree(response.getBody().toString());
        } catch (IOException e) {
            throw new HttpMessageNotReadableException("Error reading response json");
        }
        Boolean profileStoreExists = root.get("siteExists").asBoolean();
        if (!profileStoreExists) {
            return "doesn't exist";
        } else {
            log.debug("Exit from getList Parent ApiKey");
            return root.get("parentKey").textValue();
        }
    }

    public JsonNode getBrandMarketByApiKey(String apiKey, String dataCentre) {
        log.info("Enter into getList Brand & Market ByApiKey {} - {}", apiKey, dataCentre);
        UriComponentsBuilder builder =
            UriComponentsBuilder.fromHttpUrl(urls.getGetParentApiKey())
                .queryParam("apiKey", apiKey)
                .queryParam("dataCentre", dataCentre);

        HttpHeaders headers = new HttpHeaders();
        headers.set("x-api-key", keys.getKey("sandboxApiKey"));
        HttpEntity<?> entity = new HttpEntity<>(headers);

        ResponseEntity response;

        try {
            response = restTemplate.getRequest(entity, builder.toUriString());
        } catch (StatusCodeNot2xxException e) {
            log.info("Non-200 returned by Gigya! Status code received: ", e);
            throw new StatusCodeNot2xxException(e.getStatusCode(), e.getStatusText());
        }

        ObjectMapper mapper = new ObjectMapper();
        JsonNode root;
        try {
            root = mapper.readTree(response.getBody().toString());
        } catch (IOException e) {
            throw new HttpMessageNotReadableException("Error reading response json");
        }
        log.debug("Exit from getList Brand & Market ByApiKey");
        return root;
    }

}
