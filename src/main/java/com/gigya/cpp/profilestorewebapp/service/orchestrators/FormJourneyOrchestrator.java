package com.gigya.cpp.profilestorewebapp.service.orchestrators;

import com.gigya.cpp.profilestorewebapp.config.KeysPropertyConfiguration;
import com.gigya.cpp.profilestorewebapp.config.UrlPropertyConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import com.gigya.cpp.profilestorewebapp.model.pojos.FormJourney;
import com.gigya.cpp.profilestorewebapp.util.CppRestTemplate;
import com.gigya.cpp.profilestorewebapp.util.exceptions.StatusCodeNot2xxException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FormJourneyOrchestrator {

    private final CppRestTemplate restTemplate;
    private final UrlPropertyConfiguration urls;
    private final KeysPropertyConfiguration keys;

    public HttpStatus addFormJourney(FormJourney request) throws HttpStatusCodeException {
        log.debug("Enter into add Form Journey");

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        headers.set("x-api-key", keys.getKey("sandboxApiKey"));

        HttpEntity<FormJourney> entity = new HttpEntity<>(request, headers);

        ResponseEntity response;
        try {
            response = restTemplate.postRequest(entity, urls.getSetFormJourney());
        } catch (StatusCodeNot2xxException e) {
            log.info("Non-200 returned by Gigya",e);
            throw new StatusCodeNot2xxException(e.getStatusCode(), e.getStatusText());
        }
        log.debug("Exit from add Form Journey");
        return response.getStatusCode();
    }
}
