package com.gigya.cpp.profilestorewebapp.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Component;
import com.gigya.cpp.profilestorewebapp.model.mappings.BrandCodeMapping;
import com.gigya.cpp.profilestorewebapp.model.mappings.LanguageMapping;
import com.gigya.cpp.profilestorewebapp.model.mappings.MarketMapping;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CsvReaderService {

    private static final String ERROR_MESSAGE = "General IO exception when reading the next csv line";

    public List<BrandCodeMapping> getAllBrands() throws FileNotFoundException {
        CSVReader reader = openCsv("static/data/brand-codes.csv");
        List<BrandCodeMapping> brandCodeList = new LinkedList<>();

        try {
            // read line by line
            String[] record = null;

            while ((record = reader.readNext()) != null) {
                BrandCodeMapping bcm = new BrandCodeMapping();
                bcm.setBrandCode(record[0]);
                bcm.setBrandName(record[1]);

                brandCodeList.add(bcm);
            }

            reader.close();

        } catch (IOException e) {
            log.error(ERROR_MESSAGE, e);
            return brandCodeList;
        }
        return brandCodeList;
    }

    public List<LanguageMapping> getAllLanguage() throws FileNotFoundException {
        CSVReader reader = openCsv("static/data/language-codes.csv");
        Set<LanguageMapping> languageMappings = new TreeSet<>(Comparator.comparing(LanguageMapping::getEnglishName));

        try {
            // read line by line
            String[] record = null;
            while ((record = reader.readNext()) != null) {
                languageMappings.add(new LanguageMapping(record[1], record[0]));
            }
        } catch (IOException e) {
            log.error(ERROR_MESSAGE, e);
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                log.error("Unable to close the csv reader", e);
            }
        }
        return new ArrayList<>(languageMappings);
    }

    public Set<BrandCodeMapping> getAllMarketBrand(String marketCode) throws FileNotFoundException {
        CSVReader reader = openCsv("static/data/brand-pos-rules.csv");
        Set<BrandCodeMapping> brandCodeList = new LinkedHashSet<>();
        try {
            // read line by line
            String[] record = null;

            while ((record = reader.readNext()) != null) {
                if (null != record[5] && record[5].equalsIgnoreCase("MK000003") &&
                        null != record[4] && record[4].equalsIgnoreCase("ACTIVE")
                        && null != record[0] && record[0].equalsIgnoreCase(marketCode)) {
                    BrandCodeMapping bcm = new BrandCodeMapping();
                    bcm.setBrandCode(record[2]);
                    bcm.setBrandName(record[3]);
                    brandCodeList.add(bcm);
                }
            }
            reader.close();

        } catch (IOException e) {
            log.error(ERROR_MESSAGE, e);
            return brandCodeList;
        }
        return brandCodeList;
    }

    public List<MarketMapping> getAllMarkets() throws FileNotFoundException {

        CSVReader reader = openCsv("static/data/market-codes.csv");
        List<MarketMapping> marketList = new LinkedList<>();

        try {
            // read line by line
            String[] record = null;

            while ((record = reader.readNext()) != null) {
                MarketMapping marketMapping = new MarketMapping();
                marketMapping.setCountryCode(record[0]);
                marketMapping.setMarket(record[1]);

                marketList.add(marketMapping);
            }

            // Below two line will be remove in future , once getList access for Russia and China site creation
            marketList.removeIf(t -> t.getCountryCode().equalsIgnoreCase("cn"));
            marketList.removeIf(t -> t.getCountryCode().equalsIgnoreCase("ru"));

            reader.close();

        } catch (IOException e) {
            log.error(ERROR_MESSAGE, e);
            return marketList;
        }
        return marketList;
    }

    public String getDataCenterMarkets(String marketCode) throws FileNotFoundException {
        log.debug("Searching for dataCentre for marker: {}", marketCode);
        CSVReader reader = openCsv("static/data/gigya-dc-mapping.csv");
        String dataCenter = null;
        try {
            // read line by line
            String[] record = null;
            while ((record = reader.readNext()) != null) {
                if (null != record[8] && record[8].equalsIgnoreCase(marketCode)) {
                    dataCenter = record[10];
                    break;
                }
            }
            reader.close();
        } catch (IOException e) {
            log.error(ERROR_MESSAGE, e);
            return dataCenter;
        }
        return dataCenter;
    }

    @SneakyThrows
    private CSVReader openCsv(String filepath) {
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource resource = resolver.getResource("classpath:" + filepath);

        InputStream is = resource.getInputStream(); //getClass().getResourceAsStream(filepath);

        CSVParser parser = new CSVParserBuilder()
                .withSeparator(',')
                .withIgnoreQuotations(true)
                .build();

        return new CSVReaderBuilder(new InputStreamReader(is))
                .withSkipLines(1)
                .withCSVParser(parser)
                .build();
    }
}
