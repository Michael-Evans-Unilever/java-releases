package com.gigya.cpp.profilestorewebapp.service;

import com.gigya.cpp.profilestorewebapp.config.KeysPropertyConfiguration;
import com.gigya.cpp.profilestorewebapp.config.UrlPropertyConfiguration;
import com.gigya.cpp.profilestorewebapp.model.pojos.Answers;
import com.gigya.cpp.profilestorewebapp.model.pojos.Categories;
import com.gigya.cpp.profilestorewebapp.model.pojos.Question;
import com.gigya.cpp.profilestorewebapp.model.pojos.Questions;
import com.gigya.cpp.profilestorewebapp.util.CppRestTemplate;
import com.gigya.cpp.profilestorewebapp.util.exceptions.StatusCodeNot2xxException;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@Slf4j
@RequiredArgsConstructor
public class QuestionAnswerService {

    private final CppRestTemplate restTemplate;
    private final UrlPropertyConfiguration urls;
    private final KeysPropertyConfiguration keys;

    private HttpEntity<?> entity;

    public List<Question> getAllQuestionForCategory(String categoryText, String languageCode) {

        log.debug("::Enter into getList All QuestionOa3 For Category");

        UriComponentsBuilder builder =
            UriComponentsBuilder.fromUriString(urls.getGetAllQusForCategory())
                .queryParam("categoryText", categoryText)
                .queryParam("languageCode", languageCode);

        ResponseEntity response;
        try {
            Questions questions = restTemplate.get(getHttpEntity(), builder.toUriString(), new ParameterizedTypeReference<Questions>() {
            });
            return questions.getQuestions();
        } catch (StatusCodeNot2xxException e) {
            log.info("Non-200 returned by Gigya! Status code received", e);
            throw new StatusCodeNot2xxException(e.getStatusCode(), e.getStatusText());
        } finally {
            log.debug("Exit into getList All QuestionOa3 For Category");
        }
    }

    public Categories getAllQuestionCategories() {
        log.debug("::Enter into getAllQuestionCategory");
        try {
            return restTemplate.get(getHttpEntity(), urls.getGetAllQuesCategory(), new ParameterizedTypeReference<Categories>() {
            });
        } catch (StatusCodeNot2xxException e) {
            log.error("Non-200 returned by Gigya! Status code received: ", e);
            throw new StatusCodeNot2xxException(e.getStatusCode(), e.getStatusText());
        } finally {
            log.debug("Exit into getList All QuestionOa3 Category");
        }
    }

    public Answers getAllAnswersForLanguage(String languageCode) {

        log.debug("::Enter into getAllAnswersForLanguage");

        UriComponentsBuilder builder =
            UriComponentsBuilder.fromUriString(urls.getGetAllAnsForlang())
                .queryParam("languageCode", languageCode);
        ResponseEntity response;
        try {
            return restTemplate.get(getHttpEntity(), builder.toUriString(), new ParameterizedTypeReference<Answers>() {
            });
        } catch (StatusCodeNot2xxException e) {
            log.error("Non-200 returned by Gigya! Status code received", e);
            throw new StatusCodeNot2xxException(e.getStatusCode(), e.getStatusText());
        } finally {
            log.debug("::Exit into getAllAnswersForLanguage");
        }
    }

    private HttpEntity getHttpEntity() {
        if (entity == null) {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("Content-Type", "application/json");
            httpHeaders.add("x-api-key", keys.getKey("qusAnsApiKey"));
            entity = new HttpEntity<>(httpHeaders);
        }
        return entity;
    }

    public void setHttpEntity(HttpEntity entity) {
        this.entity = entity;
    }
}