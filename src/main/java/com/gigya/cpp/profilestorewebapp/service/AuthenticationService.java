package com.gigya.cpp.profilestorewebapp.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigya.cpp.profilestorewebapp.config.CustomAuthentication;
import com.gigya.cpp.profilestorewebapp.config.OidcAuthentication;
import com.gigya.cpp.profilestorewebapp.config.UrlPropertyConfiguration;
import com.gigya.cpp.profilestorewebapp.model.pojos.AwsSecretKeys;
import com.gigya.cpp.profilestorewebapp.model.pojos.AzureKeys;
import com.gigya.cpp.profilestorewebapp.model.pojos.JwtAuthenticationPayload;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@Slf4j
@RequiredArgsConstructor
public class AuthenticationService implements AuthenticationProvider {

    private final RestTemplate restTemplate;
    private final SecretsHandler secretsHandler;
    private final UrlPropertyConfiguration urlPropertyConfiguration;
    private final ObjectMapper objectMapper;

    private List<String> localAzureADPublicKeyIDs = new ArrayList<>();
    private static final List<GrantedAuthority> USER_AUTHORITIES = new ArrayList<>();
    private static final List<GrantedAuthority> ADMIN_AUTHORITIES = new ArrayList<>();

    static {
        USER_AUTHORITIES.add(new SimpleGrantedAuthority("ROLE_USER"));
        ADMIN_AUTHORITIES.add(new SimpleGrantedAuthority("ROLE_USER"));
        ADMIN_AUTHORITIES.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
    }


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        if (authentication instanceof OidcAuthentication) {
            AwsSecretKeys awsSecretKeys = secretsHandler.getSecretsFromAws();
            OidcAuthentication oidcAuthentication = (OidcAuthentication) authentication;
            if (localAzureADPublicKeyIDs.isEmpty()) {
                updateLocalAzurePublicKeyIDs(awsSecretKeys);
            }
            JWT jwt = new JWT();
            DecodedJWT decodedJWT = (jwt.decodeJwt(oidcAuthentication.getAccessToken()));
            boolean localKeyIDMatch = localAzureADPublicKeyIDs.contains(decodedJWT.getKeyId());

            /*Checks if the kid matches, the algorithm used to sign the token is correct, the token has not expired
            and that the token has been issued from the correct provider*/
            if (localKeyIDMatch
                && decodedJWT.getAlgorithm().equals(awsSecretKeys.getSigningAlgorithm())
                && decodedJWT.getExpiresAt().getTime() >= System.currentTimeMillis()
                && decodedJWT.getIssuer().equals(awsSecretKeys.getTokenIssuer())) {

                Base64 base64Url = new Base64(true);
                String tokenPayload = new String(base64Url.decode(decodedJWT.getPayload()));

                String uniqueIdentifier;
                try {
                    JwtAuthenticationPayload authPayload = objectMapper.readValue(tokenPayload, JwtAuthenticationPayload.class);
                    oidcAuthentication.setAuthenticated(true);
                    oidcAuthentication.setAuthorities(USER_AUTHORITIES);
                    if (authPayload.getUniqueName() != null) {
                        uniqueIdentifier = authPayload.getUniqueName();
                    } else {
                        uniqueIdentifier = authPayload.getEmail();
                    }
                    log.info("::Authentication Success For User :: {}", uniqueIdentifier);
                } catch (IOException e) {
                    log.error("Parse exception on JWT payload", e);
                    oidcAuthentication.setAuthenticated(false);
                    throw new BadCredentialsException("There was a problem with logging you in, please try again");
                }
            } else {
                if (!localKeyIDMatch) {
                    log.error("JWT Key ID does not match any Key IDs from Azure, cannot authenticate the user");
                } else if (!decodedJWT.getAlgorithm().equals(awsSecretKeys.getSigningAlgorithm())) {
                    log.error("An incorrect algorithm for signing the token has been used, rejecting the authentication");
                } else if (decodedJWT.getExpiresAt().getTime() < System.currentTimeMillis()) {
                    log.error("The token has an expiry date in the past, rejecting the authentication");
                } else if (!(decodedJWT.getIssuer().equals(awsSecretKeys.getTokenIssuer()))) {
                    log.error("The issuer ID does not match the one expected, rejecting the authentication");
                } else {
                    log.error("An Unknown Error has occurred");
                }
                oidcAuthentication.setAuthenticated(false);
                throw new BadCredentialsException("There was a problem with logging you in, please try again");
            }

            return oidcAuthentication;
        }
        if (authentication instanceof CustomAuthentication) {

            CustomAuthentication udhdAuthentication = (CustomAuthentication) authentication;

            if (authentication.isAuthenticated()) {
                if (udhdAuthentication.getName().contains("@unilever.com")) {
                    log.info("Admin user has logged in");
                    udhdAuthentication.setAuthorities(ADMIN_AUTHORITIES);
                } else {
                    udhdAuthentication.setAuthorities(USER_AUTHORITIES);
                }
                return udhdAuthentication;
            } else {
                throw new BadCredentialsException("There was a problem with logging you in, please try again");
            }

        }
        throw new HttpClientErrorException(HttpStatus.FORBIDDEN, "Unknown Authentication Method used");
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }


    private void updateLocalAzurePublicKeyIDs(AwsSecretKeys awsSecretKeys) {

        log.info("Could not match Key ID with local store or local store was empty, updating keys from Microsoft");

        localAzureADPublicKeyIDs = new ArrayList<>();
        URI uriSsoHost = URI.create(urlPropertyConfiguration.getAzureSsoHost());
        UriComponentsBuilder keysUri = UriComponentsBuilder.fromUri(uriSsoHost).path(awsSecretKeys.getTenant() + "/discovery/keys");

        ResponseEntity<AzureKeys> response = restTemplate.getForEntity(keysUri.toUriString(), AzureKeys.class);

        if (response.getBody() != null) {
            response.getBody().getKeys().stream().forEach(key -> localAzureADPublicKeyIDs.add(key.getKid()));
        }
    }
}
