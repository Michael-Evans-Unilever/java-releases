package com.gigya.cpp.profilestorewebapp.service;

import com.gigya.cpp.profilestorewebapp.config.AzurePropertyConfiguration;
import com.gigya.cpp.profilestorewebapp.model.pojos.AwsSecretKeys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Profile("local")
@RequiredArgsConstructor
public class LocalSecretsHandler implements SecretsHandler {

    private final AzurePropertyConfiguration azurePropertyConfiguration;
    private AwsSecretKeys awsSecretKeys;

    @Override
    public AwsSecretKeys getSecretsFromAws() {
        if (awsSecretKeys == null) {
            awsSecretKeys = AwsSecretKeys.builder()
                .clientSecret(azurePropertyConfiguration.getKey("clientSecret"))
                .profileStorePassword(azurePropertyConfiguration.getKey("profileStorePassword"))
                .profileStoreUsername(azurePropertyConfiguration.getKey("profileStoreUsername"))
                .clientID(azurePropertyConfiguration.getKey("clientID"))
                .signingAlgorithm(azurePropertyConfiguration.getKey("signingAlgorithm"))
                .state(azurePropertyConfiguration.getKey("state"))
                .tokenIssuer(azurePropertyConfiguration.getKey("tokenIssuer"))
                .tenant(azurePropertyConfiguration.getKey("tenant"))
                .build();
        }
        return awsSecretKeys;
    }
}
