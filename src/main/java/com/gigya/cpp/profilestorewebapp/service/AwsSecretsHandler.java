package com.gigya.cpp.profilestorewebapp.service;

import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.model.AWSSecretsManagerException;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigya.cpp.profilestorewebapp.model.pojos.AwsSecretKeys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Profile("!local")
@RequiredArgsConstructor
public class AwsSecretsHandler implements SecretsHandler {

    private final ObjectMapper mapper;
    private final AWSSecretsManager awsSecretsManager;

    private AwsSecretKeys awsSecretKeys;

    @Override
    public AwsSecretKeys getSecretsFromAws() {
        log.debug("Enter SecretsHandler getSecrets");
        if (awsSecretKeys == null) {
            GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest().withSecretId("cps-webapp-sso-client-secrets");
            GetSecretValueResult getSecretValueResult = awsSecretsManager.getSecretValue(getSecretValueRequest);
            String secretKeys = getSecretValueResult.getSecretString();
            try {
                log.debug("Secrets retrieved");
                awsSecretKeys = mapper.readValue(secretKeys, AwsSecretKeys.class);
            } catch (Exception e) {
                log.error("Failed to get secrets", e);
                throw new AWSSecretsManagerException("Could not retrieve AWS Secrets");
            } finally {
                log.debug("Exit SecretsHandler getSecrets");
            }
        }
        return awsSecretKeys;
    }
}
