package com.gigya.cpp.profilestorewebapp.service;


import com.gigya.cpp.profilestorewebapp.model.pojos.AwsSecretKeys;

public interface SecretsHandler {

    AwsSecretKeys getSecretsFromAws();

}
