package com.gigya.cpp.profilestorewebapp.config;

import java.util.HashMap;
import java.util.Map;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Data
@Configuration
@ConfigurationProperties("azure")
@Profile("local")
public class AzurePropertyConfiguration {

    private Map<String, String> sso = new HashMap<>();

    public String getKey(String key) {
        return sso.get(key);
    }
}
