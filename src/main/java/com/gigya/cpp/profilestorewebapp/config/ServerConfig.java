package com.gigya.cpp.profilestorewebapp.config;

import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.InstantDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.ZonedDateTimeSerializer;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatterBuilder;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ServerConfig {

    private static final String REGION = "eu-west-1";

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(Include.NON_NULL);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
        JavaTimeModule javaTimeModule = new JavaTimeModule();
        ZonedDateTimeSerializer zonedDateTimeSerializer = new ZonedDateTimeSerializer(new DateTimeFormatterBuilder().appendInstant(0).toFormatter());
        javaTimeModule.addSerializer(ZonedDateTime.class, zonedDateTimeSerializer);
        javaTimeModule.addDeserializer(ZonedDateTime.class, InstantDeserializer.ZONED_DATE_TIME);
        objectMapper.registerModule(javaTimeModule);
        return objectMapper;
    }

    @Bean
    public AWSSecretsManager getAWSSecretsManager() {
        AWSSecretsManager awsSecretsManager = AWSSecretsManagerClientBuilder.standard().withRegion(REGION)
            .build();
        return awsSecretsManager;
    }

    public class AbsoluteSendRedirectFilter implements Filter {

        public void init(FilterConfig filterConfig) throws ServletException {
        }

        public void destroy() {
        }

        public void doFilter(ServletRequest request,
            ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
            //continue the request
            chain.doFilter(request, new SendRedirectOverloadedResponse(request, response));
        }
    }

    public class SendRedirectOverloadedResponse extends HttpServletResponseWrapper {

        private HttpServletRequest httpServletRequest;
        private String prefix = null;

        public SendRedirectOverloadedResponse(ServletRequest inRequest,
            ServletResponse response) {
            super((HttpServletResponse) response);
            httpServletRequest = (HttpServletRequest) inRequest;
            prefix = getPrefix((HttpServletRequest) inRequest);
        }

        public void sendRedirect(String location) throws IOException {
            String finalurl = null;
            if (isUrlAbsolute(location)) {
                finalurl = location;
            } else {
                finalurl = fixForScheme(prefix + location);
            }
            super.sendRedirect(finalurl);
        }

        public boolean isUrlAbsolute(String url) {
            String lowercaseurl = url.toLowerCase();
            if (lowercaseurl.startsWith("http") == true) {
                return true;
            } else {
                return false;
            }
        }

        public String fixForScheme(String url) {
            //alter the url here if you were to change the scheme return url;
            return url;
        }

        public String getPrefix(HttpServletRequest request) {
            StringBuffer str = request.getRequestURL();
            String url = str.toString();
            String uri = request.getRequestURI();
            int offset = url.indexOf(uri);
            String prefix = url.substring(0, offset);
            return prefix;
        }
    }
}
