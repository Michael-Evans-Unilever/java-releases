package com.gigya.cpp.profilestorewebapp.config;

import static com.gigya.cpp.profilestorewebapp.model.pojos.CppConstants.SESSION_ID;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;

@Component
public class MdcLogEnhancerFilter implements Filter {

    @Override
    public void destroy() {
        MDC.remove(SESSION_ID);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        MDC.put(SESSION_ID, RequestContextHolder.currentRequestAttributes().getSessionId());
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
