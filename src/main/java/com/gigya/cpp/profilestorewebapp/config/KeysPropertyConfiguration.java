package com.gigya.cpp.profilestorewebapp.config;

import java.util.HashMap;
import java.util.Map;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("secrets")
public class KeysPropertyConfiguration {

    private Map<String, String> keys = new HashMap<>();

    public String getKey(String key) {
        return keys.get(key);
    }
}
