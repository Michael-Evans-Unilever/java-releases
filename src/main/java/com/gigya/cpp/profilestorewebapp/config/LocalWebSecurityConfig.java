package com.gigya.cpp.profilestorewebapp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import com.gigya.cpp.profilestorewebapp.service.AuthenticationService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Profile("local")
public class LocalWebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    AuthenticationService authProvider;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .authorizeRequests()
                .antMatchers("/", "/css/**", "/img/**", "/public/js/libs/**", "/js/**", "/ssoLogin", "/error", "/health", "/info").permitAll()
                .anyRequest().authenticated()
                .and()
                .authenticationProvider(authProvider)
                .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/udhdLogin")
                .defaultSuccessUrl("/myProfileStore")
                .failureUrl("/login?error")
                .permitAll()
                .and()
                .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login")
                .permitAll()
                .and().csrf()
                .ignoringAntMatchers("/ssoLogin")
                .and()
                .sessionManagement().sessionFixation().none();

    }
}
