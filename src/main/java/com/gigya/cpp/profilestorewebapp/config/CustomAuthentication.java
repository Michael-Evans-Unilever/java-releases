package com.gigya.cpp.profilestorewebapp.config;

import java.util.ArrayList;
import java.util.List;
import javax.security.auth.Subject;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import lombok.Data;

@Component
@Data
public class CustomAuthentication implements Authentication {

  private static final long serialVersionUID = 7787361731998244684L;
  private boolean authenticated;
  private List<GrantedAuthority> authorities;
  private String username;

  public CustomAuthentication() {
    authenticated = false;
    authorities = new ArrayList<>();
  }

  @Override
  public Object getCredentials() {
    return null;
  }

  @Override
  public Object getDetails() {
    return null;
  }

  @Override
  public Object getPrincipal() {
    return null;
  }

    @Override
    public String getName() {
        return username;
    }
  @Override
  public boolean implies(Subject subject) {
    return false;
  }
}

