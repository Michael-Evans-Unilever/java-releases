package com.gigya.cpp.profilestorewebapp.config;

import java.util.ArrayList;
import java.util.List;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

@Component
public class OidcAuthentication implements Authentication {

    private static final long serialVersionUID = -4876337416492185692L;
    private String accessToken;
    private boolean authenticated;
    private List<GrantedAuthority> authorities;

    public OidcAuthentication(String accessToken) {
        this.accessToken = accessToken;
        this.authenticated = false;
        this.authorities = new ArrayList<>();
    }

    public OidcAuthentication() {
        this.accessToken = null;
        this.authenticated = false;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    @Override
    public List<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean b) throws IllegalArgumentException {
        authenticated = b;

    }

    @Override
    public String getName() {
        return null;
    }
}
