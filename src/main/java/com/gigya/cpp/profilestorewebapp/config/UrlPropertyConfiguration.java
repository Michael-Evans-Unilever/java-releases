package com.gigya.cpp.profilestorewebapp.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("url")
@Data
public class UrlPropertyConfiguration {

    private String udhdUrl;
    private String createProfileStore;
    private String setFormJourney;
    private String getParentApiKey;
    private String getAllQusForCategory;
    private String getAllQuesCategory;
    private String getAllAnsForlang;
    private String getSpecificQuestion;
    private String getSpecificAnswer;
    private String azureSsoHost;
    private String azureSsoRedirectUri;
}
