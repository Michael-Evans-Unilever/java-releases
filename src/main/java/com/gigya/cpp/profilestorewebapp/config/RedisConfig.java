package com.gigya.cpp.profilestorewebapp.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.session.data.redis.config.ConfigureRedisAction;

@Configuration
@Profile("!local")
public class RedisConfig {

    @Value("${spring.redis.cluster.nodes:'localhost'}")
    private String redisClusterConfigurationEndpoint;

    @Value("${spring.redis.cluster.port:30001}")
    private int redisPort;


    @Bean
    public static ConfigureRedisAction ConfigureRedisAction() {
        return ConfigureRedisAction.NO_OP;
    }

    @Bean
    public RedisConnectionFactory redisConnectionFactory() {
        RedisConnectionFactory redisConnectionFactory = new JedisConnectionFactory(redisClusterConfiguration(), jedisClientConfiguration());
        return redisConnectionFactory;
    }

    @Bean
    public RedisClusterConfiguration redisClusterConfiguration() {
        RedisClusterConfiguration redisClusterConfiguration = new RedisClusterConfiguration();
        redisClusterConfiguration.clusterNode(redisClusterConfigurationEndpoint, redisPort);
        return redisClusterConfiguration;
    }

    @Bean
    public JedisClientConfiguration jedisClientConfiguration() {

        JedisClientConfiguration jedisClientConfiguration = JedisClientConfiguration.builder()
            .useSsl().build();

        return jedisClientConfiguration;
    }

    @Bean
    public RedisTemplate redisTemplate() {
        RedisTemplate redisTemplate = new RedisTemplate();
        redisTemplate.setConnectionFactory(redisConnectionFactory());
        return redisTemplate;
    }

}
