package com.gigya.cpp.profilestorewebapp.exception.handlers;

import com.gigya.cpp.generated.profilestorewebapp.model.CppError;
import com.gigya.cpp.generated.profilestorewebapp.model.CppFail;
import com.gigya.cpp.generated.profilestorewebapp.model.DetailedResponseBody;
import com.gigya.cpp.profilestorewebapp.exception.CppException;
import java.util.Arrays;
import javax.annotation.Priority;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
@Slf4j
@Priority(1)
public class CppExceptionHandler extends ResponseEntityExceptionHandler implements AbstractExceptionHandler {

    @ExceptionHandler(value = CppException.class)
    public final ResponseEntity<Object> cppFailResponse(CppException cppException) {

        log.info("Constructing CppException Error Response");
        if (cppException.getExceptionDetail() instanceof CppFail) {
            CppFail cppFail = (CppFail) cppException.getExceptionDetail();
            return createResponseEntity(
                (String) cppFail.getStatus(),
                (int) cppFail.getStatusCode(),
                (String) cppFail.getMessage(),
                cppFail.getBody());
        }
        if (cppException.getExceptionDetail() instanceof CppError) {
            CppError cppError = (CppError) cppException.getExceptionDetail();
            return createResponseEntity(
                (String) cppError.getStatus(),
                (int) cppError.getStatusCode(),
                (String) cppError.getMessage(),
                cppError.getBody());
        }

        return createResponseEntity("ERROR", 500, "INTERNAL_SERVER_ERROR",
            Arrays.asList(new DetailedResponseBody()
                .number(500301)
                .code("ERROR_GENERATING_ERROR_RESPONSE")));


    }


}
