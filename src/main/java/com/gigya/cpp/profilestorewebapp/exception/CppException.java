package com.gigya.cpp.profilestorewebapp.exception;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigya.cpp.generated.profilestorewebapp.model.CppError;
import com.gigya.cpp.generated.profilestorewebapp.model.CppFail;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CppException extends RuntimeException {

    @Getter
    private Object exceptionDetail;

    public CppException(CppError error) {
        super(toJsonString(error));
        exceptionDetail = error;
    }

    public CppException(CppFail fail) {
        super(toJsonString(fail));
        exceptionDetail = fail;
    }

    @SneakyThrows
    private static String toJsonString(Object o) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(Include.NON_NULL);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper.writeValueAsString(o);
    }


}
