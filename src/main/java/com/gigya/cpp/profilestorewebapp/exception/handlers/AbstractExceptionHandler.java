package com.gigya.cpp.profilestorewebapp.exception.handlers;

import com.gigya.cpp.generated.profilestorewebapp.model.DetailedResponseBody;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

public interface AbstractExceptionHandler {

    default ResponseEntity<Object> createResponseEntity(String status,
        int statusCode,
        String message,
        List<DetailedResponseBody> detailedResponseBody) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("status", status);
        body.put("statusCode", statusCode);
        body.put("message", message);
        body.put("body", detailedResponseBody);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity<>(body, httpHeaders, HttpStatus.valueOf(statusCode));

    }
}
