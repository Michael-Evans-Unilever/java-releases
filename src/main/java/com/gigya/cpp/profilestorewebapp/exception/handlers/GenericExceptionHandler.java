package com.gigya.cpp.profilestorewebapp.exception.handlers;

import com.gigya.cpp.generated.profilestorewebapp.model.DetailedResponseBody;
import java.util.Arrays;
import javax.annotation.Priority;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
@Slf4j
@Priority(100000)
public class GenericExceptionHandler extends ResponseEntityExceptionHandler implements AbstractExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public final ResponseEntity<Object> exceptionResponse(Exception exception) {
        log.info("Constructing Exception Error Response {}", exception);
        String message = exception == null ? "Unkown" : exception.getMessage();
        return createResponseEntity("ERROR", 500, "INTERNAL_SERVER_ERROR",
            Arrays.asList(new DetailedResponseBody()
                .number(500001)
                .code("UNKNOWN_ERROR_CODE")
                .field(message)));
    }

}
