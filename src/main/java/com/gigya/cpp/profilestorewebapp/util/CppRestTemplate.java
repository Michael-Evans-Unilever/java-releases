package com.gigya.cpp.profilestorewebapp.util;

import com.gigya.cpp.generated.profilestorewebapp.model.CppError;
import com.gigya.cpp.generated.profilestorewebapp.model.DetailedResponseBody;
import com.gigya.cpp.profilestorewebapp.config.KeysPropertyConfiguration;
import com.gigya.cpp.profilestorewebapp.config.UrlPropertyConfiguration;
import com.gigya.cpp.profilestorewebapp.exception.CppException;
import com.gigya.cpp.profilestorewebapp.model.pojos.TaskResponse;
import com.gigya.cpp.profilestorewebapp.service.SecretsHandler;
import java.nio.charset.Charset;
import java.util.Base64;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import com.gigya.cpp.profilestorewebapp.util.exceptions.StatusCodeNot2xxException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CppRestTemplate {

  private final RestTemplate restTemplate;
  private final SecretsHandler secretsHandler;
  private final UrlPropertyConfiguration urls;
  private final KeysPropertyConfiguration keys;
  private HttpEntity<?> httpEntity;

  @Setter
  private HttpHeaders httpHeaders;

  public HttpHeaders getHttpHeaders() {
    if (httpHeaders == null) {
      httpHeaders = new HttpHeaders();
      httpHeaders.add("Content-Type", "application/json");
      httpHeaders.add("x-api-key", keys.getKey("sandboxApiKey"));
    }
    return httpHeaders;
  }

  private HttpEntity<?> getHttpEntity() {
    if (httpEntity == null) {
      httpEntity = new HttpEntity<>(getHttpHeaders());
    }
    return httpEntity;
  }

  private <T> HttpEntity<T> getHttpEntity(T request) {
    HttpEntity<T> requestHttpEntity = new HttpEntity<T>(request, getHttpHeaders());
    return requestHttpEntity;
  }

  public <R> HttpEntity<R> getHttpEntity(R request, String authUser, String authPassword, String userId) {
    HttpHeaders httpHeaders = new HttpHeaders();
    String auth = authUser + ":" + authPassword;
    byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(Charset.forName("UTF-8")) );
    String authHeader = "Basic " + new String( encodedAuth );
    httpHeaders.add("Content-Type", "application/json");
    httpHeaders.add("x-api-key",  keys.getKey("sandboxApiKey"));
    httpHeaders.add("Authorization", authHeader);
    if (StringUtils.isNoneEmpty(userId)) {
      httpHeaders.add("X-Username", userId);
    }

    HttpEntity<R> requestHttpEntity = new HttpEntity<R>(request, httpHeaders);
    return requestHttpEntity;
  }

  public ResponseEntity getRequest(String url) {
    ResponseEntity response;

    try {
      response = restTemplate.exchange(url, HttpMethod.GET, getHttpEntity(), String.class);
    } catch (HttpStatusCodeException e) {
      throw new CppException(new CppError()
          .status("ERROR")
          .statusCode(e.getStatusCode().value())
          .message("INTERNAL_SERVER_ERROR")
          .addBodyItem(new DetailedResponseBody().number(500302).code("FAILURE_WHILE_SENDING_REQUEST")));
    }

    return response;
  }

  public <T> T get(String url, ParameterizedTypeReference<T> parameterizedTypeReference) {
    final ResponseEntity<T> response = restTemplate.exchange(
        url,
        HttpMethod.GET,
        getHttpEntity(),
        parameterizedTypeReference
    );
    return response.getBody();
  }

  public <R, T> ResponseEntity<T> postUsingServiceNameWithHttpEntity(HttpEntity<R> request, String serviceName, Class<T> expectedClass) {
    String url = urls.getCreateProfileStore();
    log.debug("Attempting to send a request to {} with request params {}", url, request);
    final ResponseEntity<T> response = restTemplate.exchange(
        url,
        HttpMethod.POST,
        request,
        expectedClass
    );
    return response;
  }

  public <R, T> ResponseEntity<T> postUsingServiceNameWithRawResponse(R request, String serviceName, Class<T> expectedClass) {
    String url = urls.getCreateProfileStore();
    HttpEntity<R> entity = getHttpEntity(request);

    log.debug("Attempting to send a request to {} with request params {}", url, request);
    log.debug("The request headers look like: {}", entity);
    final ResponseEntity<T> response = restTemplate.exchange(
        url,
        HttpMethod.POST,
        entity,
        expectedClass
    );
    return response;
  }

  public <R> void postUsingServiceNameWithStandardTaskResponse(R request, String serviceName) {
    postUsingServiceNameWithRawResponse(request, serviceName, TaskResponse.class);
  }

  public <R, T> T postUsingServiceName(R request, String serviceName, Class<T> expectedClass) {
    ResponseEntity<T> response = postUsingServiceNameWithRawResponse(request, serviceName, expectedClass);
    return response.getBody();
  }




  public ResponseEntity postRequest(HttpEntity request, String url) throws StatusCodeNot2xxException {
    ResponseEntity response;

    try {
      response = restTemplate.postForEntity(url, request, String.class);
    } catch (HttpStatusCodeException e) {
      throw new StatusCodeNot2xxException(e.getStatusCode(), e.getStatusText());
    }

    return response;
  }

  public ResponseEntity getRequest(HttpEntity request, String url) throws StatusCodeNot2xxException {
    ResponseEntity response;

    try {
      response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
    } catch (HttpStatusCodeException e) {
      throw new StatusCodeNot2xxException(e.getStatusCode(), e.getStatusText());
    }

    return response;
  }

  public <T> T get(HttpEntity request, String url, ParameterizedTypeReference<T> parameterizedTypeReference) {
    final ResponseEntity<T> response = restTemplate.exchange(
        url,
        HttpMethod.GET,
        request,
        parameterizedTypeReference
    );
    return response.getBody();
  }

  public <T> ResponseEntity<T> post(HttpEntity request, String url, Class<T> expectedClass ) {
    final ResponseEntity<T> response = restTemplate.exchange(
        url,
        HttpMethod.POST,
        request,
        expectedClass
    );
    return response;
  }
}
