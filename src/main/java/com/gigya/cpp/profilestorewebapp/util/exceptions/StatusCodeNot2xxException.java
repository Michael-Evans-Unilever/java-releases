package com.gigya.cpp.profilestorewebapp.util.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;

public class StatusCodeNot2xxException extends HttpStatusCodeException {

    public StatusCodeNot2xxException(HttpStatus statusCode, String statusText) {
        super(statusCode, statusText);
    }
}
