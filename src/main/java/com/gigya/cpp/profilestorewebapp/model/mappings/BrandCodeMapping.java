package com.gigya.cpp.profilestorewebapp.model.mappings;

import lombok.Data;

@Data
public class BrandCodeMapping {

    private String brandName;
    private String brandCode;
}
