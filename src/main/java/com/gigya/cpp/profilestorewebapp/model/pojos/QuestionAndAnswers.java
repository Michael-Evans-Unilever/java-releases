package com.gigya.cpp.profilestorewebapp.model.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.gigya.cpp.profilestorewebapp.model.pojos.QuestionAndAnswers.QuestionAndAnswersRequestBuilder;
import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@ToString
@EqualsAndHashCode
@Builder(builderClassName = "QuestionAndAnswersRequestBuilder", toBuilder = true)
@JsonDeserialize(builder = QuestionAndAnswersRequestBuilder.class)
@Data
public class QuestionAndAnswers {

    private String categories;
    @JsonProperty("qaquestion")
    private String question;

    @Builder.Default
    private List<String> answer = new ArrayList<>();
    private String questionText;

    @JsonPOJOBuilder(withPrefix = "")
    public static class QuestionAndAnswersRequestBuilder {

    }
}