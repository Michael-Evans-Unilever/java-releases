package com.gigya.cpp.profilestorewebapp.model.pojos;


import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@AllArgsConstructor
@Getter
public enum WebAppEnvironments {
  LOCAL(false, "local", "LocalDev", "#434342", 100, false),
  DEVELOPMENT(false, "development", "Development", "#F34342", 152, false),
  UAT(false, "qa", "UAT-QA", "#21A320", 88, true),
  SANDBOX(false, "sandbox", "Sandbox", "#21A320", 100, true),
  PROD(true, "production", "", "", 0, true),
  UNKNOWN(false, "UNKNOWN", "Unknown", "#FF0303", 132, true);

  private boolean prod;
  private String springProfile;
  private String friendlyName;
  private String watermarkColour;
  private int width;
  private boolean usePackedJs;

  public static WebAppEnvironments fromSpringProfile(String springProfile) {
    if (StringUtils.isNotBlank(springProfile)) {
      return Arrays.stream(values()).filter(e -> e.springProfile.equalsIgnoreCase(springProfile)).findFirst().orElse(WebAppEnvironments.UNKNOWN);
    }
    return UNKNOWN;
  }
}
