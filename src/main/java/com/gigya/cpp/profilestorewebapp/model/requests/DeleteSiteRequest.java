package com.gigya.cpp.profilestorewebapp.model.requests;

public class DeleteSiteRequest {

    private String partnerID;

    public String getPartnerID() {
        return partnerID;
    }

    public void setPartnerID(String partnerID) {
        this.partnerID = partnerID;
    }
}
