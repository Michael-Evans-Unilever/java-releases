package com.gigya.cpp.profilestorewebapp.model.pojos;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class Question {
    private String questionId;
    private String languageCode;

    @EqualsAndHashCode.Exclude
    private String questionText;
}
