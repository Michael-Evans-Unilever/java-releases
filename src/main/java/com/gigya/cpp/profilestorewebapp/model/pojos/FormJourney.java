package com.gigya.cpp.profilestorewebapp.model.pojos;

import lombok.Data;

@Data
public class FormJourney {

    private String apiKey;
    private String screenset;
    private String market;
    private String formJourney;
    private String language;
    private String brandCode;
    private String dataCentre;
    private String terms;

}
