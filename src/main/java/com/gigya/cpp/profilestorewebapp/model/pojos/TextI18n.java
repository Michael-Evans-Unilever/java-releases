package com.gigya.cpp.profilestorewebapp.model.pojos;

import lombok.Data;

@Data
public class TextI18n {

  private Long parentId;
  private String iso2CountryCode;
  private String iso3CountryCode;
  private String text;

}
