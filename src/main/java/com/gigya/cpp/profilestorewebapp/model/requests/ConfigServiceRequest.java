package com.gigya.cpp.profilestorewebapp.model.requests;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.gigya.cpp.profilestorewebapp.model.pojos.ServiceIds;
import com.gigya.cpp.profilestorewebapp.model.requests.ConfigServiceRequest.ConfigServiceRequestBuilder;
import java.util.List;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode
@ToString
@Builder(builderClassName = "ConfigServiceRequestBuilder")
@JsonDeserialize(builder = ConfigServiceRequestBuilder.class)
public class ConfigServiceRequest {

  private final String parentKey;
  private final String childKey;
  private final String dataCentre;
  private final String brandName;
  private final String brandCode;
  private final String url;
  private final String country;
  private final String language;
  private final ServiceIds serviceIds;


  @JsonPOJOBuilder(withPrefix = "")
  public static class ConfigServiceRequestBuilder {

  }
}
