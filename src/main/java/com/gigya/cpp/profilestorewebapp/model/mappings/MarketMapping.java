package com.gigya.cpp.profilestorewebapp.model.mappings;

import lombok.Data;

@Data
public class MarketMapping {

    private String market;
    private String countryCode;
}
