package com.gigya.cpp.profilestorewebapp.model.pojos;

import java.util.List;
import lombok.Data;

@Data
public class QuestionOa3 {

  private Long id;
  private String category;
  private String categoryCode;
  private String sensitivity;
  private String sensitivityCode;
  private List<TextI18n> questionTextList;
}
