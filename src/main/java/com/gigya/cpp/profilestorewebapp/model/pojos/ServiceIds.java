package com.gigya.cpp.profilestorewebapp.model.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.gigya.cpp.profilestorewebapp.model.pojos.ServiceIds.ServiceIdsBuilder;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@Builder(builderClassName = "ServiceIdsBuilder", toBuilder = true)
@JsonDeserialize(builder = ServiceIdsBuilder.class)
public class ServiceIds {

  private String email;
  private String post;
  private String sms;
  private String mms;
  private String phone;

  @Singular
  private List<String> newsletters;

  @JsonPOJOBuilder(withPrefix = "")
  public static class ServiceIdsBuilder {

  }
}
