package com.gigya.cpp.profilestorewebapp.model.pojos;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@JsonDeserialize(builder = AzureKeys.AzureKeysBuilder.class)
@Builder(builderClassName = "AzureKeysBuilder", toBuilder = true)
public class AzureKeys {
    private List<AzureDiscoveredKeys> keys;

    @JsonPOJOBuilder(withPrefix = "")
    public static class AzureKeysBuilder {

    }
}
