package com.gigya.cpp.profilestorewebapp.model.pojos;

import java.util.List;
import lombok.Data;

@Data
public class AnswerOa3 {

  private Long id;
  private String category;
  private String categoryCode;
  private List<TextI18n> answerTextList;
}
