package com.gigya.cpp.profilestorewebapp.model.pojos;


import lombok.Data;

@Data
public class TaskResponse {

    private String message;
}
