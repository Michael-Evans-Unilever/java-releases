package com.gigya.cpp.profilestorewebapp.model.pojos;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.gigya.cpp.profilestorewebapp.model.pojos.JwtAuthenticationPayload.JwtAuthenticationPayloadBuilder;
import lombok.Builder;
import lombok.Data;

@Data
@JsonDeserialize(builder = JwtAuthenticationPayloadBuilder.class)
@Builder(builderClassName = "JwtAuthenticationPayloadBuilder", toBuilder = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class JwtAuthenticationPayload {

    private String email;

    @JsonProperty("unique_name")
    private String uniqueName;


    @JsonPOJOBuilder(withPrefix = "")
    public static class JwtAuthenticationPayloadBuilder {

    }
}
