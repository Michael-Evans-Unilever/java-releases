package com.gigya.cpp.profilestorewebapp.model.requests;

public class AddChildSiteRequest {

    private String apiKey;
    private String brand;
    private String campaignId;
    private String emailServiceId;
    private String newsletterServiceId;
    private String postServiceId;
    private String url;
    private String doubleOptIn;
    private String terms;
    private String privacy;
    private String cookie;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getEmailServiceId() {
        return emailServiceId;
    }

    public void setEmailServiceId(String emailServiceId) {
        this.emailServiceId = emailServiceId;
    }

    public String getNewsletterServiceId() {
        return newsletterServiceId;
    }

    public void setNewsletterServiceId(String newsletterServiceId) {
        this.newsletterServiceId = newsletterServiceId;
    }

    public String getPostServiceId() {
        return postServiceId;
    }

    public void setPostServiceId(String postServiceId) {
        this.postServiceId = postServiceId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDoubleOptIn() {
        return doubleOptIn;
    }

    public void setDoubleOptIn(String doubleOptIn) {
        this.doubleOptIn = doubleOptIn;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }
}
