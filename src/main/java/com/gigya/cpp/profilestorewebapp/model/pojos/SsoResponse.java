package com.gigya.cpp.profilestorewebapp.model.pojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import java.net.URI;
import lombok.Builder;
import lombok.Data;


/**
 * A pojo used to deserialise the response from an Azure AD request.
 * A successful response ooks like:
 * <pre>
 *       {
 *         "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6Ik5HVEZ2ZEstZnl0aEV1Q...",
 *         "token_type": "Bearer",
 *         "expires_in": 3599,
 *         "scope": "https%3A%2F%2Fgraph.microsoft.com%2Fuser.read",
 *         "refresh_token": "AwABAAAAvPM1KaPlrEqdFSBzjqfTGAMxZGUTdM0t4B4...",
 *         "id_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJhdWQiOiIyZDRkMTFhMi1mODE0LTQ2YTctOD...",
 *     }
 * </pre>
 */
@Data
@JsonDeserialize(builder = SsoResponse.SsoResponseBuilder.class)
@Builder(builderClassName = "SsoResponseBuilder", toBuilder = true)
public class SsoResponse {

    @JsonProperty("access_token")
    private String accessToken;

    @JsonProperty("token_type")
    private String tokenType;

    @JsonProperty("expires_in")
    private Long expiresIn;

    @JsonProperty("scope")
    private String scope;

    @JsonProperty("refresh_token")
    private String refreshToken;

    @JsonProperty("id_token")
    private String idToken;

    @JsonPOJOBuilder(withPrefix = "")
    public static class SsoResponseBuilder {

    }

}
