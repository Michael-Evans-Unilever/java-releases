package com.gigya.cpp.profilestorewebapp.model.pojos;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.gigya.cpp.profilestorewebapp.model.pojos.AzureDiscoveredKeys.AzureDiscoveredKeysBuilder;
import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@JsonDeserialize(builder = AzureDiscoveredKeysBuilder.class)
@Builder(builderClassName = "AzureDiscoveredKeysBuilder", toBuilder = true)
public class AzureDiscoveredKeys {

    private String kty;
    private String sig;
    private String kid;
    private String x5t;
    private String n;
    private String e;
    private List<String> x5c;

    @JsonPOJOBuilder(withPrefix = "")
    public static class AzureDiscoveredKeysBuilder {

    }
}
