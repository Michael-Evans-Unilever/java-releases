package com.gigya.cpp.profilestorewebapp.model.pojos;

import lombok.Data;

@Data
public class ProfileStoreDTO {

    private String environment;
    private String parentApiKey;
    private String childApiKey;
    private String profileStoreStatus;
    private String environmentDesc;
}
