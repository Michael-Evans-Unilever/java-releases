package com.gigya.cpp.profilestorewebapp.model.mappings;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class LanguageMapping {
    private final String englishName;
    private final String isoCode;

    public LanguageMapping(String englishName, String isoCode) {
        this.englishName = englishName.trim();
        this.isoCode = isoCode.trim();
    }

}
