package com.gigya.cpp.profilestorewebapp.model.pojos;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Singular;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class Answers {

  @Singular
  List<Answer> answers;
}
