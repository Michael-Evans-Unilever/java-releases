package com.gigya.cpp.profilestorewebapp.model.pojos;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;

@Data
@JsonDeserialize(builder = AwsSecretKeys.AwsSecretKeysBuilder.class)
@Builder(builderClassName = "AwsSecretKeysBuilder", toBuilder = true)
public class AwsSecretKeys {

    private final String profileStoreUsername;
    private final String profileStorePassword;
    private final String tenant;
    private final String clientID;
    private final String clientSecret;
    private final String tokenIssuer;
    private final String signingAlgorithm;
    private final String state;

    @JsonPOJOBuilder(withPrefix = "")
    public static class AwsSecretKeysBuilder {

    }
}
