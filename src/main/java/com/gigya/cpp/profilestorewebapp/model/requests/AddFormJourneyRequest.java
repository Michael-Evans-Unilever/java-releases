package com.gigya.cpp.profilestorewebapp.model.requests;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.gigya.cpp.profilestorewebapp.model.pojos.QuestionAndAnswers;
import com.gigya.cpp.profilestorewebapp.model.requests.AddFormJourneyRequest.AddFormJourneyRequestBuilder;
import java.util.List;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Singular;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@ToString
@EqualsAndHashCode
@Builder(builderClassName = "AddFormJourneyRequestBuilder", toBuilder = true)
@JsonDeserialize(builder = AddFormJourneyRequestBuilder.class)
public class AddFormJourneyRequest {

  private String siteApiKey;
  private String market;
  @JsonProperty
  private List<String> language;
  private String brandCode;
  private String dataCentre;
  private String templateSelect;
  private String terms;
  private String formJourney;

  @Singular("addQuestionAndAnswer")
  private List<QuestionAndAnswers> qanda;

  @JsonPOJOBuilder(withPrefix = "")
  public static class AddFormJourneyRequestBuilder {

  }
}