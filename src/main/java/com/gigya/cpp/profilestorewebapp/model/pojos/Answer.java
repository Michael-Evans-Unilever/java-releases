package com.gigya.cpp.profilestorewebapp.model.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class Answer {
    private String answerId;
    private String languageCode;

    @EqualsAndHashCode.Exclude
    private String answerText;
}
