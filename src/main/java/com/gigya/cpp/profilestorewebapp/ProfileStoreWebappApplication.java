package com.gigya.cpp.profilestorewebapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProfileStoreWebappApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProfileStoreWebappApplication.class, args);
    }
}
