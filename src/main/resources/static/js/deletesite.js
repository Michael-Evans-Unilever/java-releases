$(document).ready(function(){
    $("#deleteSite").submit(function(e){

        e.preventDefault();

        var form = $('#deleteSite');
        var formMessages = $('#form-messages');

        // Serialize the form into a string of JSON
        var formData = getFormDataAsJson(form);

        $(formMessages).append("<p> Finding Sites.. <p>");

        $.ajax({
            type: 'POST',
            url: '/deleteSite',
            headers: {
                "Content-Type": "application/json",
                "X-CSRF-Token": $('[name="_csrf"]').val()
            },
            data: formData
        })
        .done(function(response) {
            // Make sure that the formMessages div has the 'success' class.
            $(formMessages).removeClass('error');
            $(formMessages).addClass('success');

            // Set the message text.
            $(formMessages).append("<p>"+ response + "</p>");

        })
        .fail(function(data) {
            // Make sure that the formMessages div has the 'error' class.
            $(formMessages).removeClass('success');
            $(formMessages).addClass('error');

            // Set the message text.
            if (data.responseText !== '') {
                $(formMessages).append("<p>"+ data.responseText + "</p>");
            } else {
                $(formMessages).append('<p> Oops! An error occured and your message could not be sent. </p>');
            }
        });
    });
});
