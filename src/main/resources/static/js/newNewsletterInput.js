(function() {
    const plusIcon = document.getElementById("new-newsletter-icon");
    const newsletterContainer = document.getElementById("newsletter-inputs-container");

    plusIcon.addEventListener('click', function(){
        addNewsletterInput()
    });

    function addNewsletterInput() {
        var container = document.createElement("div"),
            newInput = document.createElement("input"),
            span = document.createElement("span");

        container.className = "position-relative";

        newInput.type = "text";
        newInput.name = "serviceIdNewsletters";
        newInput.className = "form-control newsletterInput";
        newInput.placeholder = "Enter a Newsletter Service ID";
        newInput.maxLength = "64";
        newInput.pattern = "^[a-zA-Z0-9_]+$";
        newInput.addEventListener('keydown', formHelperTouchedEvent);
        newInput.addEventListener('blur', formHelperTouchedEvent);
        newInput.addEventListener('blur', formHelperEmptyEvent);
        container.appendChild(newInput);
        container.appendChild(span);
        newsletterContainer.appendChild(container);
    }

    function formHelperTouchedEvent() {
        $(this).addClass('touched');
    }


    function formHelperEmptyEvent() {
        if ($(this).val() === "") {
            $(this).addClass('empty');
        } else {
            $(this).removeClass('empty');
        }
    }
})();
