(function() {
    const answerIcon = document.getElementById("new-answer-icon");
    $('#new-answer-icon').click(function(){
       var index = $('#new-answer-icon').index();
       if(index===5){
           alert("Reached maximum answer for selected question")
           return false;
       }else{
            $('#answer').clone().attr('id','answer_'+$('#new-answer-icon').index()).insertAfter('#answer');
       }
    });

})();
