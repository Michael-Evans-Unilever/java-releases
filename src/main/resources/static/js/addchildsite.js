$(document).ready(function(){

    $('input').one('blur keydown', function() {
        $(this).addClass('touched');
    }).on('blur', function() {
        if ($(this).val() === "") {
            $(this).addClass('empty');
        } else {
            $(this).removeClass('empty');
        }
    });


    $("#addChildSite").submit(function(e){




        e.preventDefault();



        var form = $('#addChildSite'),
            formMessages = $('#form-messages'),
            consoleLog = $('[data-console-log]');

        // Serialize the form into a string of JSON
        var formData = getFormDataAsJson(form);


        formMessages.parent().removeClass('d-none').addClass('d-block');
        formMessages.append("<p> Adding Child Site.. <p>");
        consoleLog.pulse({backgroundColor : '#ffe4b5'}, { duration : 3250, pulses   : -1, interval : 800});

        $.ajax({
            type: 'POST',
            url: '/addChildSite',
            headers: {
                "Content-Type": "application/json",
                "X-CSRF-Token": $('[name="_csrf"]').val()
            },
            data: formData
        })
        .done(function(response) {
            // Make sure that the formMessages div has the 'success' class.
            $(formMessages).removeClass('error');
            $(formMessages).addClass('success');

            // Set the message text.
            $(formMessages).append("<p>"+ response + "</p>");
            consoleLog.pulse('destroy');
        })
        .fail(function(data) {
            // Make sure that the formMessages div has the 'error' class.
            $(formMessages).removeClass('success');
            $(formMessages).addClass('error');

            // Set the message text.
            if (data.responseText !== '') {
                $(formMessages).append("<p>"+ data.responseText + "</p>");
            } else {
                $(formMessages).append('<p> Oops! An error occured and your message could not be sent. </p>');
            }
            consoleLog.pulse('destroy');

        });
    });
});
