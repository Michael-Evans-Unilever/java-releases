(function() {
    const apiKeyInput = document.getElementById("apiKey");
    // this script runs on multiple pages, so check element exists first
    if (apiKeyInput) {
        apiKeyInput.addEventListener("input", function(event) {
            if (apiKeyInput.validity.patternMismatch) {
                apiKeyInput.setCustomValidity("API Key can only contain letters, numbers, hyphens and underscores, and must be 66 characters long. Please check your API Key.");
            }
            // this else block is required, otherwise when the user corrects the input, the error message will remain
            else {
                apiKeyInput.setCustomValidity("");
            }
        });
    }

    const campaignIdInput = document.getElementById("campaignId");
    if (campaignIdInput) {
        campaignIdInput.addEventListener("input", function(event) {
            if (campaignIdInput.validity.patternMismatch) {
                campaignIdInput.setCustomValidity("Campaign ID can only contain letters and numbers");
            } else {
                campaignIdInput.setCustomValidity("");
            }
        });
    }

    const types = ["email", "post", "sms", "mms", "phone"];
    for (var i = 0; i < types.length; i++) {
        const type = types[i];
        const serviceIdInput = document.getElementById(type + "ServiceId");
        if (serviceIdInput) {
            serviceIdInput.addEventListener("input", function(event) {
                const serviceId = serviceIdInput.value;
                // https://stackoverflow.com/questions/52783125/eventlistener-that-triggers-when-text-input-but-not-when-input-is-deleted/
                if (serviceId.length && !isValidServiceId(serviceId, type)) {
                    // backticks below denote a 'template string':  https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals
                    // The free version of IntelliJ doesn't pick this syntax up as a string.
                    //template literals don't work in IE11
                    var validType = (type==="email") ? "EML" : type.toUpperCase();
                    serviceIdInput.setCustomValidity(type.toUpperCase() + " service id must be in this format: eg. SVC_GB_AB1234_"+validType);
                } else {
                    serviceIdInput.setCustomValidity("");
                }
            })
        }
    }

    /* when this file runs (when the page has loaded), the user won't have expanded the newsletters yet, and therefore when he/she
     * does, a new event listener has to be added to the new newsletter input. i.e. we can't add them now
     * (at page load) because they don't all exist yet */
    const icon = document.getElementById("new-newsletter-icon");
    if (icon) {
        icon.addEventListener("click", function(event) {
            addNewsletterEventListeners();
        });
    }

    addNewsletterEventListeners();


    function addNewsletterEventListeners() {
        const newsletterInputs = document.getElementsByClassName("newsletterInput");
        for (var i = 0; i < newsletterInputs.length; i++) {
            const input = newsletterInputs[i];
            if (input) {
                input.addEventListener("input", function(event) {
                    const serviceId = input.value;
                    if (serviceId.length && !isValidServiceId(serviceId, "newsletter")) {
                        input.setCustomValidity("Newsletter Service Id must be in this format: eg. SVC_GB_AB1234_EML_Newsletter");
                    } else {
                        input.setCustomValidity("");
                    }
                });
            }
        }
    }

    /**
     * Checks if Service Id is valid.
     * Returns boolean.
     *
     * @param {string} service id.
     * @param {string} the type of serviceId: email, post, sms, mms, phone
     */
    function isValidServiceId(serviceId, type) {

        const serviceIdComponents = serviceId.split('_');

        if (type.toLowerCase() === "newsletter") {
            return !(serviceIdComponents.length < 5                     ||            // at least 5 parts, separated by '_'
                !RegExp(/^(SVC|BRAND)$/).test(serviceIdComponents[0])   ||            // 0: SVC or BRAND
                !RegExp(/^[A-Z]{2}$/).test(serviceIdComponents[1])      ||            // 1: 2 uppercase letters
                !RegExp(/^[A-Z0-9]{6}$/).test(serviceIdComponents[2])   ||            // 2: 6 chars
                !RegExp(/^(EML|POST|SMS|MMS|PHONE)$/).test(serviceIdComponents[3]))   // 3: EML / POST / SMS / MMS / PHONE
                                                                                      // 4 onwards: can be anything
        }
        else {
            if (type.toLowerCase() === "email") type = "EML";
            return !(serviceIdComponents.length !== 4                   ||  // 4 parts, separated by '_'
                !RegExp(/^(SVC|BRAND)$/).test(serviceIdComponents[0])   ||  // 0: SVC or BRAND
                !RegExp(/^[A-Z]{2}$/).test(serviceIdComponents[1])      ||  // 1: 2 uppercase letters
                !RegExp(/^[A-Z0-9]{6}$/).test(serviceIdComponents[2])   ||  // 2: 6 chars
                serviceIdComponents[3] !== type.toUpperCase())              // 3: EML / POST / SMS / MMS / PHONE

        }
    }

    /*
 *  Project: jQuery Light Weight MultiSelect
 *  Version: 1.1.0
 *  Date: 7/11/2013
 *  Requires: jQuery 1.7+
 *  Description: A light weight plugin that transforms a multi select drop menu into two panels for easy selections.
 *  Demo: http://bennylin.net/jquery-plugin-light-weight-multiselect/
 *  Author: Benny Lin - http://bennylin.net
 *  Designer: Monique Aviles
 *  License: MIT License
 *  jQuery Boilerplate: http://jqueryboilerplate.com/
 */

    (function ($, window, document, undefined) {
        var pluginName = "lwMultiSelect";
        var defaults = {
            addAllText: "Select All",
            removeAllText: "Remove All",
            selectedLabel: "Values accepted",
            maxSelect: 0, //0 = no restrictions
            maxText: '',
            onChange: ''
        };

        // The actual plugin constructor
        function Plugin(element, options) {
            this.$element = $(element);
            if (options) {
                options.maxSelect = parseInt(options.maxSelect) || defaults.maxSelect;
            }
            this.options = $.extend({}, defaults, options);
            this._defaults = defaults;
            this._name = pluginName;
            this.totalItem = 0; //cache total count
            this.$mainContainer = "";
            this.$leftDiv = "";
            this.$rightDiv = "";
            this.$leftHead = "";
            this.$rightHead = "";
            this.$availList = "";
            this.$selectedList = "";
            this.$counter = "";
            this.init();
        }

        Plugin.prototype = {
            init: function () {
                var tmphtml = "";
                this.$element.hide();
                this.$mainContainer = $('<div class="lwms-main lwms-cf"></div>').insertAfter(this.$element);
                this.$leftDiv = $('<div class="lwms-left"></div>').appendTo(this.$mainContainer);
                this.$rightDiv = $('<div class="lwms-right"></div>').appendTo(this.$mainContainer);

                if (this.options.maxSelect != 0) {
                    tmphtml = '<div class="lwms-filterhead"><div class="lwms-maxtext">'+ this.options.maxText +'</div></div>';
                } else {
                    tmphtml = '<div class="lwms-filterhead"><a href="#" class="lwms-addall">' + this.options.addAllText + '</a>&nbsp; | &nbsp;<a href="#" class="lwms-removeall">' + this.options.removeAllText + '</a></div>';
                }
                this.$leftHead = $(tmphtml).appendTo(this.$leftDiv);
                this.$rightHead = $('<div class="lwms-filterhead"><span class="lwms-filcount"></span> '+this.options.selectedLabel+'</div>').appendTo(this.$rightDiv);
                this.$availList = $('<ul class="lwms-list lwms-available"></ul>').appendTo(this.$leftDiv);
                this.$selectedList = $('<ul class="lwms-list"></ul>').appendTo(this.$rightDiv);
                this.$counter = this.$mainContainer.find('.lwms-filcount');

                this.updateList();
                this.initEvents();
            },

            //return if current selected is not more than max select
            isBelowMax: function () {
                return (this.$selectedList.children().length < this.options.maxSelect);
            },

            /* event: clicking on left container li */
            selectItem: function (that) {
                if (this.options.maxSelect == 0 || this.isBelowMax()) { //allow selection if there's no max or selection is below max
                    var $self = $(that);
                    $self.clone().appendTo(this.$selectedList); //clone the element and append to selected, this is required due to search visibility
                    $self.addClass('lwms-selected'); //lwms-selected is to preserved visibility state of search filters
                    this.$element.find('option[value="' + $self.data('value') + '"]').attr('selected', 'selected'); //mark selected on the source, used attr vs prop because clone doesn't carry over selected attr
                    this.updateCount(); //refresh counts
                    this.triggerChange(); //trigger change callback
                }
            },

            /* event: clicking on right container li */
            removeItem: function(that) {
                var $self = $(that);
                this.$availList.find('li[data-value="' + $self.data('value') + '"]').removeClass('lwms-selected'); //remove lwms-selected from available list
                this.$element.find('option[value="' + $self.data('value') + '"]').removeAttr('selected'); //remove selected on the source
                $self.remove(); //remove the current element from selected
                this.updateCount(); //refresh counts
                this.triggerChange(); //trigger change callback
            },

            /* event: selecting all visible */
            selectAll: function () {
                if (this.options.maxSelect == 0) { //only allow this if no max specified
                    var tmpArrId, $tempAddList = this.$availList.find('li:visible'); //cache list of visible items only (search filters and items not already selected)

                    this.$selectedList.append($tempAddList.clone()); //clone the list and append to selected

                    tmpArrId = $tempAddList.map(function () {
                        return $(this).data('value').toString(); //convert this to string, data values are typed
                    }).get(); //get an array of selected values

                    //update the source select
                    this.$element.find('option').filter(function (index) {
                        return ($.inArray(this.value, tmpArrId) > -1); //inArray checks with ===, we need tmpArrId to be a array of strings
                    }).attr('selected', 'selected'); //used attr vs prop because clone doesn't carry over selected attr

                    $tempAddList.addClass('lwms-selected'); //mark them with lwms-selected

                    this.updateCount();
                    this.triggerChange();
                }
            },

            /* event: remove all */
            removeAll: function() {
                this.$availList.find('li').removeClass('lwms-selected'); //remove all lwms-selected from available
                this.$selectedList.empty(); //clear selected list
                this.$element.children().removeAttr('selected'); //remove all selected from source, used attr vs prop because clone doesn't carry over selected attr

                this.updateCount();
                this.triggerChange();
            },

            //initial all event handlers
            initEvents: function() {
                var that = this;
                //event delegations

                /* clicking items on left container */
                this.$availList.off('click.lwmultiselect','li');
                this.$availList.on('click.lwmultiselect', 'li', function (e) {
                    e.preventDefault();
                    that.selectItem(this);
                });

                /* remove clicks */
                this.$selectedList.off('click.lwmultiselect','li');
                this.$selectedList.on('click.lwmultiselect', 'li', function (e) {
                    e.preventDefault();
                    that.removeItem(this);
                });

                //select all
                this.$mainContainer.off('click.lwmultiselect','.lwms-addall');
                this.$mainContainer.on('click.lwmultiselect','.lwms-addall',function(e) {
                    e.preventDefault();
                    that.selectAll();
                });

                //remove all
                this.$mainContainer.off('click.lwmultiselect','.lwms-removeall');
                this.$mainContainer.on('click.lwmultiselect','.lwms-removeall',function(e) {
                    e.preventDefault();
                    that.removeAll();
                });
            },

            //refresh the containers with the elements from the original source
            updateList : function() {
                var that = this,
                    tmpSelectHtml = '',
                    tmpAvailHtml = '',
                    $tmpThis,
                    tmpVal,
                    tmpText,
                    selectClass = '';

                this.$selectedList.empty(); //clear available and selected list
                this.$availList.empty();
                this.totalItem = this.$element.children().length; //cache the total per update

                //loop through the source and rebuild the list
                this.$element.children().each(function () {
                    selectClass = '';
                    $tmpThis = $(this);
                    tmpVal = $tmpThis.prop('value');
                    tmpText = $tmpThis.text();

                    //if option is preselected then append to selected and add lwms-selected to the available side
                    if ($tmpThis.is(':selected')) {
                        tmpSelectHtml += '<li class="lwms-selectli" data-value="' + tmpVal + '">' + tmpText + '</li>';
                        selectClass = ' lwms-selected';
                    }
                    tmpAvailHtml += '<li class="lwms-selectli' + selectClass + '" data-value="' + tmpVal + '">' + tmpText + '</li>';
                });

                this.$selectedList.html(tmpSelectHtml);
                this.$availList.html(tmpAvailHtml);
                this.updateCount();
            },

            /* call back function if defined */
            triggerChange: function() {
                if (this.options.onChange != '') {
                    this.options.onChange();
                }
            },

            /* refresh count */
            updateCount: function () {
                this.$counter.text(this.$selectedList.children().length + '/' + ((this.options.maxSelect == 0 ) ? this.totalItem : this.options.maxSelect));
            }
        };

        // A really lightweight plugin wrapper around the constructor,
        // preventing against multiple instantiations
        $.fn[pluginName] = function (options) {
            return this.each(function () {
                if (!$.data(this, "plugin_" + pluginName)) {
                    $.data(this, "plugin_" + pluginName, new Plugin(this, options));
                }
            });
        };
    })(jQuery, window, document);

    $('#answer').lwMultiSelect({
        addAllText:'Add all answers',
        removeAllText:'Clear all',
        selectedLabel:'Selected answers',
        maxSelect: 5,
        maxText: 'Select up to 5'
    });

    $('#language').lwMultiSelect({
        selectedLabel:'Selected languages'
    });
})();
