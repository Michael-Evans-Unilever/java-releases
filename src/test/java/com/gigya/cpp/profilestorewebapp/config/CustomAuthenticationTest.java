package com.gigya.cpp.profilestorewebapp.config;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import javax.security.auth.Subject;
import java.util.Collections;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class CustomAuthenticationTest {

    private CustomAuthentication customAuthentication;

    @Before
    public void init() {
        customAuthentication = new CustomAuthentication();
    }

    @Test
    public void isAuthenticatedTest() {
        assertFalse(customAuthentication.isAuthenticated());
    }

    @Test
    public void setAuthenticatedTest() {
        customAuthentication.setAuthenticated(true);
        assertTrue(customAuthentication.isAuthenticated());
        customAuthentication.setAuthenticated(false);
        assertFalse(customAuthentication.isAuthenticated());
    }

    @Test
    public void getAuthoritiesTest() {
        assertEquals(Collections.emptyList(), customAuthentication.getAuthorities());
    }

    @Test
    public void getCredentialsTest() {
        assertNull(customAuthentication.getCredentials());
    }

    @Test
    public void getDetailsTest() {
        assertNull(customAuthentication.getDetails());
    }

    @Test
    public void getPrincipalTest() {
        assertNull(customAuthentication.getPrincipal());
    }

    @Test
    public void impliesTest() {
        assertFalse(customAuthentication.implies(new Subject()));
    }

    @Test
    public void equalsTest() {
        assertNotEquals(customAuthentication, new Object());
    }

    @Test
    public void toStringTest() {
        customAuthentication.setUsername("eric@bananaman.com");
        SimpleGrantedAuthority sga = new SimpleGrantedAuthority("TEST_ROLE");
        customAuthentication.setAuthorities(Collections.singletonList(sga));
        assertEquals("CustomAuthentication(authenticated=false, authorities=[TEST_ROLE], username=eric@bananaman.com)", customAuthentication.toString());
    }

    @Test
    public void hashCodeTest() {
        customAuthentication.setUsername("eric@bananaman.com");
        SimpleGrantedAuthority sga = new SimpleGrantedAuthority("TEST_ROLE");
        customAuthentication.setAuthorities(Collections.singletonList(sga));
        assertEquals(-1266074879, customAuthentication.hashCode());
    }

    @Test
    public void getNameTest() {
        assertNull(customAuthentication.getName());
    }
}