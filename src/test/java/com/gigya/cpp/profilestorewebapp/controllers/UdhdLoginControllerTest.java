package com.gigya.cpp.profilestorewebapp.controllers;

import com.gigya.cpp.profilestorewebapp.config.UrlPropertyConfiguration;
import com.gigya.cpp.profilestorewebapp.model.pojos.AwsSecretKeys;
import com.gigya.cpp.profilestorewebapp.service.AuthenticationService;
import com.gigya.cpp.profilestorewebapp.service.SecretsHandler;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UdhdLoginControllerTest {

  private MockMvc mockMvc;

  @Mock
  private Model model;

  @Mock
  private UrlPropertyConfiguration mockUrlPropertyConfiguration;

  @Mock
  private RestTemplate mockRestTemplate;

  @Mock
  private AuthenticationService authenticationService;

  @Mock
  private RequestAttributes attrs;

  private UdhdLoginController loginController;

  @Before
  public void init() {
    MockitoAnnotations.initMocks(model);
    loginController = new UdhdLoginController(mockRestTemplate, authenticationService, mockUrlPropertyConfiguration);
    mockMvc = MockMvcBuilders.standaloneSetup(loginController).build();
    when(mockUrlPropertyConfiguration.getUdhdUrl()).thenReturn("https://udhd/LdapAuthenticationService.svc/Json/IsAuthenticated/username/password");
    Authentication auth = mock(AnonymousAuthenticationToken.class);
    SecurityContextHolder.getContext().setAuthentication(auth);
  }

  @After
  public void tearDown() {
  }

  @Test
  public void contextLoads() {
    assertNotNull(loginController);
  }

  @Test
  public void successfulUdhdAuthorisation() {
    Mockito.when(mockRestTemplate.getForEntity(anyString(), any(), any(Class.class))).thenReturn(new ResponseEntity<>("true", HttpStatus.OK));
    RequestContextHolder.setRequestAttributes(attrs);

    assertEquals("redirect:/myProfileStore", loginController.udhdLogin(new MockHttpSession(), new MockHttpServletRequest(), "test", "test"));
  }

  @Test
  public void failedUdhdAuthorisation() {
    Mockito.when(mockRestTemplate.getForEntity(anyString(), any(), any(Class.class))).thenReturn(new ResponseEntity<>("false", HttpStatus.OK));
    RequestContextHolder.setRequestAttributes(attrs);
    assertEquals("redirect:/login?error",
        loginController.udhdLogin(new MockHttpSession(), new MockHttpServletRequest(), "Test", "Test"));
  }

  @Test(expected = InternalError.class)
  public void errorUdhdAuthorisation() {
    Mockito.when(mockRestTemplate.getForEntity(anyString(), any(), any(Class.class))).thenReturn(new ResponseEntity<>("false", HttpStatus.BAD_REQUEST));
    RequestContextHolder.setRequestAttributes(attrs);
    loginController.udhdLogin(new MockHttpSession(), new MockHttpServletRequest(), "test", "test");
  }


  private AwsSecretKeys setUpTestSecretsMap() {
    AwsSecretKeys awsSecretKeys = AwsSecretKeys.builder()
        .clientSecret("testSecret")
        .clientID("testClient")
        .signingAlgorithm("testAlgorithm")
        .state("testState")
        .tokenIssuer("testToken")
        .tenant("testTenant")
        .profileStorePassword("")
        .profileStoreUsername("")
        .build();

    return awsSecretKeys;
  }
}