package com.gigya.cpp.profilestorewebapp.controllers;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigya.cpp.generated.profilestorewebapp.model.CreationStatus;
import com.gigya.cpp.generated.profilestorewebapp.model.Environment;
import com.gigya.cpp.generated.profilestorewebapp.model.ProfileRequest;
import com.gigya.cpp.generated.profilestorewebapp.model.ProfileStoreBody;
import com.gigya.cpp.generated.profilestorewebapp.model.ProfileStoreResponse;
import com.gigya.cpp.profilestorewebapp.config.KeysPropertyConfiguration;
import com.gigya.cpp.profilestorewebapp.config.UrlPropertyConfiguration;
import com.gigya.cpp.profilestorewebapp.model.pojos.FormJourney;
import com.gigya.cpp.profilestorewebapp.model.pojos.QuestionAndAnswers;
import com.gigya.cpp.profilestorewebapp.model.requests.AddFormJourneyRequest;
import com.gigya.cpp.profilestorewebapp.service.CsvReaderService;
import com.gigya.cpp.profilestorewebapp.service.ProfileStoreService;
import com.gigya.cpp.profilestorewebapp.service.QuestionAnswerService;
import com.gigya.cpp.profilestorewebapp.service.SecretsHandler;
import com.gigya.cpp.profilestorewebapp.service.orchestrators.FormJourneyOrchestrator;
import com.gigya.cpp.profilestorewebapp.util.CppRestTemplate;
import com.gigya.cpp.profilestorewebapp.util.exceptions.StatusCodeNot2xxException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.mock.http.client.MockClientHttpResponse;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletWebRequest;

@RunWith(MockitoJUnitRunner.class)
public class InternalRequestControllerTest {

    private InternalRequestController internalRequestController;

    private static final String SITE_INFO_RESPONSE = "{\n"

        + "        \"siteExists\": true,\n"
        + "        \"parentKey\": \"3_oj8agEyOdvtM7kPn49bldDaEfeZPN4v0M-ENIxuuqY56B6WiiDMguzycCvClBIM4\",\n"
        + "        \"market\": \"dz\",\n"
        + "        \"brandCode\": \"bh0107\"\n"
        + "}";



    private static final String SITE_INFO_RESPONSE1 = "{\n"
        +"    \"profileStoreExists\": true,\n"
        +"    \"dataCenter\": \"eu1\",\n"
        +"    \"ResponseMsg\": \"API Key validated successfully!\",\n"
        +"    \"Success\": true,\n"
        +"    \"brandCode\": \"bh0107\"\n"
        +"}";

    @Mock
    private ProfileStoreService profileStoreService;

    @Mock
    private CppRestTemplate restTemplate;

    @Mock
    private SecretsHandler secretsHandler;

    @Mock
    private UrlPropertyConfiguration urls;

    @Mock
    private KeysPropertyConfiguration keys;

    @Mock
    private FormJourneyOrchestrator formJourneyOrchestrator;
    @Mock
    private QuestionAnswerService questionAnswerService;
    @Mock
    private CsvReaderService csvReaderService;


    private ProfileRequest createProfileStoreRequestExample;
    private ProfileStoreResponse profileStoreResponse = new ProfileStoreResponse();
    private QuestionAndAnswers questionAndAnswersFirstList = QuestionAndAnswers.builder()
        .categories("Cooking")
        .question("2370")
        .answer(Collections.singletonList("7540"))
        .answer(Collections.singletonList("5909"))
        .questionText("Do you like cooking?")
        .build();
    private QuestionAndAnswers questionAndAnswersSecondList = QuestionAndAnswers.builder()
        .categories("Personal Care")
        .question("2880")
        .answer(Collections.singletonList("7520"))
        .answer(Collections.singletonList("5919"))
        .questionText("Do you cook daily?")
        .build();
    private AddFormJourneyRequest addFormJourneyRequest = AddFormJourneyRequest.builder()
        .siteApiKey("testSiteApiKey")
        .market("testMarket")
        .language(asList("testLanguage1", "testLanguage2"))
        .brandCode("testBrandCode")
        .dataCentre("testDataCentre")
        .terms("testTerms")
        .formJourney("testFormJourneyOne")
        .addQuestionAndAnswer(questionAndAnswersFirstList)
        .addQuestionAndAnswer(questionAndAnswersSecondList)
        .build();

    private static final String EMPTY_LIST = "";
    private static final String SINGLE_FORM_JOURNEY = "Screen1";


    @Before
    public void init() {
        RequestAttributes mockRequest = new ServletWebRequest(new MockHttpServletRequest("GET", "/test"));
        RequestContextHolder.setRequestAttributes(mockRequest);

        internalRequestController = new InternalRequestController(profileStoreService, formJourneyOrchestrator, questionAnswerService, csvReaderService);

        createProfileStoreRequestExample = new ProfileRequest()
            .brandCode("testBrandCode")
            .campaignId("testCampaignId")
            .cookiesLink(URI.create("testCookiesLink"))
            .countryCode("testCountry")
            .doubleOptInRequired(true)
            .languageCode("en")
            .privacyLink(URI.create("testPrivacyLink"))
            .serviceIdEmail("testIdEmail")
            .serviceIdMms("testIdMms")
            .serviceIdNewsletters(asList("testIdNewsletterOne"))
            .serviceIdPhone("testIdPhone")
            .serviceIdPost("testIdPost")
            .serviceIdSms("testIdSms")
            .termsLink(URI.create("testTerms"))
            .url(URI.create("testUrl"));

        addFormJourneyRequest = AddFormJourneyRequest.builder()
            .siteApiKey("testApiKey")
            .market("testMarket")
            .language(asList("en", "ar", "fr", "es"))
            .brandCode("testBrandCode")
            .dataCentre("eu1")
            .addQuestionAndAnswer(questionAndAnswersFirstList)
            .addQuestionAndAnswer(questionAndAnswersSecondList)
            .terms("testTerms")
            .build();

        ProfileStoreBody profileStoreBody = new ProfileStoreBody().apiKey("OneChildKey").creationStatus(CreationStatus.NEW).environment(Environment.SANDBOX);
        ProfileStoreResponse profileStoreResponse = new ProfileStoreResponse().message("OK").status("SUCCESS").statusCode(200).body(profileStoreBody);
    }

    @Test
    public void addFormJourneyIncorrectProfileStoreTest() {
        when(profileStoreService.getParentApiKey(anyString(), anyString())).thenReturn("doesn't exist");
        assertEquals("API Key doesn't exist.", internalRequestController.addFormJourney(addFormJourneyRequest));
    }

    @Test
    public void addNewFormJourneyStatusCodeExceptionTest() {
        when(profileStoreService.getParentApiKey(anyString(), anyString()))
            .thenThrow(new StatusCodeNot2xxException(HttpStatus.GATEWAY_TIMEOUT, "error"));
        assertEquals("Forms not added - contact support team",
            internalRequestController.addFormJourney(addFormJourneyRequest));
    }

    @Test
    public void addNewFormJourneyHttpMessageNotReadableExceptionTest() {
        MockClientHttpResponse response = new MockClientHttpResponse("wibble.com".getBytes(), HttpStatus.OK);
        when(profileStoreService.getParentApiKey(anyString(), anyString()))
            .thenThrow(new HttpMessageNotReadableException("error", response));
        assertEquals("Forms not added - contact support team",
            internalRequestController.addFormJourney(addFormJourneyRequest));

    }

    @Test
    public void addNewFormJourneyNoScreensGivenTest() {
        addFormJourneyRequest = addFormJourneyRequest.toBuilder().formJourney(EMPTY_LIST).build();
        when(profileStoreService.getParentApiKey(anyString(), anyString())).thenReturn("validKey");
        String expected = "All forms added successfully!</br><b>ALL FORMS : </b></br>Add the following script code into" +
            " your web page's head section :<xmp id=\"allForms\"><script type=\"text/javascript\" " +
            "src=\"https://cdns.gigya.com/JS/gigya.js?apiKey=testApiKey&lang=en-testMarket\"></script>\n" +
            "<script src=\"//cdn.gigya-ext.com/gy.js\" type=\"text/javascript\"></script> </xmp><a href=\"#\"" +
            " class=\"aClipBoard\" onClick=getCopyText(event,\"allForms\")>Copy to clipboard</a></br></br>" +
            "<p><b>FORM : </b>:</br>Add the following html code into your webpage body section form : " +
            "</p><xmp id=\"\"><div class=\"gy-ui-screen-set\" data-screen-set=\"\"></xmp><a href=\"#\" " +
            "class=\"aClipBoard\" onClick=getCopyText(event,\"\")>Copy to clipboard</a></br>";
        assertEquals(expected, internalRequestController.addFormJourney(addFormJourneyRequest));
    }

    @Test
    public void addNewFormJourneySuccessTest() {
        addFormJourneyRequest = addFormJourneyRequest.toBuilder().formJourney(SINGLE_FORM_JOURNEY).build();
        when(profileStoreService.getParentApiKey(anyString(), anyString())).thenReturn("validKey");
        when(formJourneyOrchestrator.addFormJourney(any(FormJourney.class))).thenReturn(HttpStatus.OK);
        String output = internalRequestController.addFormJourney(addFormJourneyRequest);
        String str = "All forms added successfully!</br><b>ALL FORMS : </b></br>Add the following script code into your web page's head section :"
            + "<xmp id=\"allForms\"><script type=\"text/javascript\" src=\"https://cdns.gigya.com/JS/gigya.js?apiKey=testApiKey&lang=en-testMarket\"></script>\n"
            + "<script src=\"//cdn.gigya-ext.com/gy.js\" type=\"text/javascript\"></script> </xmp><a href=\"#\" class=\"aClipBoard\" "
            + "onClick=getCopyText(event,\"allForms\")>Copy to clipboard</a></br></br><p><b>FORM : Screen1</b>:</br>Add the following html code into your webpage "
            + "body section form : </p><xmp id=\"Screen1\"><div class=\"gy-ui-screen-set\" data-screen-set=\"Screen1\"></xmp><a href=\"#\" class=\"aClipBoard\" "
            + "onClick=getCopyText(event,\"Screen1\")>Copy to clipboard</a></br>";
        assertTrue(output.contains(str));
    }

    @Test
    public void addNewFormJourneyFailureTest() {
        addFormJourneyRequest = addFormJourneyRequest.toBuilder().formJourney(SINGLE_FORM_JOURNEY).build();
        when(profileStoreService.getParentApiKey(anyString(), anyString())).thenReturn("validKey");
        when(formJourneyOrchestrator.addFormJourney(any(FormJourney.class)))
            .thenThrow(new StatusCodeNot2xxException(HttpStatus.GATEWAY_TIMEOUT, "error"));
        assertEquals("Selected Form Journey is not applicable to the Market selected.",
            internalRequestController.addFormJourney(addFormJourneyRequest));
    }


    @Test
    public void getBrandNameByAPIKeyTest() throws IOException {
        when(csvReaderService.getDataCenterMarkets(anyString())).thenReturn("eu1");
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(SITE_INFO_RESPONSE);
        when(profileStoreService.getBrandMarketByApiKey("key", "eu1")).thenReturn(root);
        Map<String, Object> response = internalRequestController.getBrandNameByAPIKey("gb", "key", null);
        assertNotNull(response);
        assertTrue((boolean)response.get("profileStoreExists"));
        assertTrue((boolean)response.get("Success"));
        assertEquals("eu1", (String)response.get("dataCenter"));
        assertEquals("API Key validated successfully!", (String)response.get("ResponseMsg"));
        assertEquals("bh0107", (String)response.get("brandCode"));
    }
}
