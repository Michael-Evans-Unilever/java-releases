package com.gigya.cpp.profilestorewebapp.controllers;

import com.gigya.cpp.profilestorewebapp.service.CsvReaderService;
import com.gigya.cpp.profilestorewebapp.service.QuestionAnswerService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletWebRequest;

import java.io.FileNotFoundException;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class PagesControllerTest {

    private MockMvc mockMvc;

    @Mock
    private Model model;

    @Mock
    private CsvReaderService mockCsvReaderService;

    @Mock
    private QuestionAnswerService questionAnswerService;

    private PagesController pagesController;

    @Before
    public void init() throws FileNotFoundException {
        RequestAttributes mockRequest = new ServletWebRequest(new MockHttpServletRequest("GET", "/test"));
        RequestContextHolder.setRequestAttributes(mockRequest);
        pagesController = new PagesController(mockCsvReaderService, questionAnswerService, true);
        mockMvc = MockMvcBuilders.standaloneSetup(pagesController).build();
    }

    @Test
    public void contextLoads() {
        assertNotNull(pagesController);
        assertNotNull(mockCsvReaderService);
    }

    @Test
    public void displayCreateProfileStorePageTest() throws Exception{
        this.mockMvc.perform(get("/createProfileStorePage")).andExpect(status().isOk());
    }

    @Test
    public void createProfileStorePageRedirectTest() throws Exception{
        Mockito.when(mockCsvReaderService.getAllMarkets()).thenThrow(new FileNotFoundException());
        assertEquals("redirect:/createProfileStorePage?error=500", pagesController.displayCreateProfileStorePage(null, model));
    }

    @Test
    public void CreateProfileStorePageErrorPathTest() throws Exception{
        this.mockMvc.perform(get("/createProfileStorePage?error=500")).andExpect(status().isOk());
        Mockito.verify(mockCsvReaderService, Mockito.times(0)).getAllMarkets();
    }

    @Test
    public void displayProfilePage() throws Exception{
        this.mockMvc.perform(get("/myProfileStore")).andExpect(status().isOk());
    }

    @Test
    @Ignore
    public void displayAddFormJourneyPage() throws Exception{
        this.mockMvc.perform(get("/addFormJourney")).andExpect(status().isOk());
    }

    @Test
    public void AddFormJourneyRedirectTest() throws Exception{
        Mockito.when(mockCsvReaderService.getAllMarkets()).thenThrow(new FileNotFoundException());
        assertEquals("redirect:/addFormJourney?error=500", pagesController.displayAddFormJourneyPage(null, null,model));
    }

    @Test
    public void AddFormJourneyErrorPathTest() throws Exception{
        this.mockMvc.perform(get("/addFormJourney?error=500")).andExpect(status().isOk());
        Mockito.verify(mockCsvReaderService, Mockito.times(0)).getAllMarkets();
    }

    @Test
    public void activeBrandList() throws Exception{
        this.mockMvc.perform(get("/getActiveBrandList")).andExpect(status().isOk());
    }

    @Test
    public void activeBrandListFailureTest() throws Exception{
        Mockito.when(mockCsvReaderService.getAllMarketBrand(anyString())).thenThrow(new FileNotFoundException());
        assertEquals(null, pagesController.activeBrandList("test", model));
    }

    @Test
    public void displayDeleteSitePage() throws Exception{
        this.mockMvc.perform(get("/deleteSitePage")).andExpect(status().isOk());
    }

    @Test
    public void displayAddChildSitePage() throws Exception{
        this.mockMvc.perform(get("/addChildSite")).andExpect(status().isOk());
    }
}