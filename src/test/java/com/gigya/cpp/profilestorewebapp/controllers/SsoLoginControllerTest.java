package com.gigya.cpp.profilestorewebapp.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.gigya.cpp.profilestorewebapp.config.UrlPropertyConfiguration;
import com.gigya.cpp.profilestorewebapp.model.pojos.AwsSecretKeys;
import com.gigya.cpp.profilestorewebapp.model.pojos.SsoResponse;
import com.gigya.cpp.profilestorewebapp.service.AuthenticationService;
import com.gigya.cpp.profilestorewebapp.service.SecretsHandler;
import java.net.URI;
import javax.naming.AuthenticationException;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.util.UriComponentsBuilder;

@RunWith(MockitoJUnitRunner.class)
public class SsoLoginControllerTest {

    private MockMvc mockMvc;

    @Mock
    private Model model;

    @Mock
    private SecretsHandler mockSecretsHandler;

    @Mock
    private RestTemplateBuilder restTemplateBuilder;

    @Mock
    private RestTemplate mockRestTemplate;

    @Mock
    private AuthenticationService authenticationService;

    @Mock
    private UrlPropertyConfiguration urlPropertyConfiguration;

    @Mock
    private RequestAttributes attrs;

    private SsoLoginController loginController;

    @Before
    public void init() {
        loginController = new SsoLoginController(mockRestTemplate, mockSecretsHandler, urlPropertyConfiguration, authenticationService);
        mockMvc = MockMvcBuilders.standaloneSetup(loginController).build();
        when(urlPropertyConfiguration.getAzureSsoHost()).thenReturn("http://test.com");
        when(urlPropertyConfiguration.getAzureSsoRedirectUri()).thenReturn("http://www.testredirect.com");
        Authentication auth = mock(AnonymousAuthenticationToken.class);
        SecurityContextHolder.getContext().setAuthentication(auth);
    }

    @After
    public void tearDown() {
        Mockito.reset(mockSecretsHandler, model);
    }

    @Test
    public void contextLoads() {
        assertNotNull(loginController);
    }

    @Test
    public void showLoginWithNoErrors() {
        assertEquals("login", loginController.showLogin(new MockHttpSession(), new MockHttpServletRequest(), model, null, null, null));
        Mockito.verify(model, Mockito.times(0)).addAttribute(anyString(), anyString());
    }

    @Test
    public void showLoginWithErrors() {
        assertEquals("login", loginController.showLogin(new MockHttpSession(), new MockHttpServletRequest(), model, "error", null, null));
        Mockito.verify(model, Mockito.times(1)).addAttribute(anyString(), anyString());
    }

    @Test
    public void showLoginWithssoErrors() {
        assertEquals("login", loginController.showLogin(new MockHttpSession(), new MockHttpServletRequest(), model, null, "ssoError", null));
        Mockito.verify(model, Mockito.times(1)).addAttribute(anyString(), anyString());
    }

    @Test
    public void showLogout() {
        assertEquals("login", loginController.showLogin(new MockHttpSession(), new MockHttpServletRequest(), model, null, null, "logout"));
        Mockito.verify(model, Mockito.times(1)).addAttribute(anyString(), anyString());
    }

    @Test
    public void redirectsIfAlreadyLoggedIn() {
        SecurityContextHolder.clearContext();
        assertEquals("redirect:/myProfileStore", loginController.showLogin(new MockHttpSession(), new MockHttpServletRequest(), model, null, null, null));
    }


    @Test
    public void successfulSsoAuthorisationRequest() {
        AwsSecretKeys awsSecretKeys = setUpTestSecretsMap();
        MultiValueMap<String, String> testParams = new LinkedMultiValueMap<>();
        testParams.add("client_id", awsSecretKeys.getClientID());
        testParams.add("response_type", "code");
        testParams.add("redirect_uri", "http://www.testredirect.com");
        testParams.add("response_mode", "form_post");
        testParams.add("scope", "openid");
        testParams.add("state", "testState");

        Mockito.when(mockSecretsHandler.getSecretsFromAws()).thenReturn(awsSecretKeys);

        UriComponentsBuilder fullUri = UriComponentsBuilder.fromUri(URI.create("http://test.com"))
            .path(awsSecretKeys.getTenant() + "/oauth2/v2.0/authorize").queryParams(testParams);

        assertTrue(loginController.ssoLogin(new MockHttpServletRequest(), new MockHttpSession()).equals("redirect:" + fullUri.toUriString()));

    }

    @Test
    public void successfulSsoTokenRequest() throws JSONException, AuthenticationException {
        SsoResponse ssoResponse = SsoResponse.builder()
            .accessToken("testAccess")
            .tokenType("Bearer")
            .expiresIn(3599L)
            .scope("openid")
            .idToken("testIDToken")
            .build();

        Mockito.when(mockSecretsHandler.getSecretsFromAws()).thenReturn(setUpTestSecretsMap());
        Mockito.when(mockRestTemplate.postForEntity(anyString(), any(), any(Class.class)))
            .thenReturn(new ResponseEntity<>(ssoResponse, HttpStatus.OK));
        assertEquals("redirect:/myProfileStore", loginController.validateToken("testIDToken", "testState", null, null));
    }

    @Test
    public void handleSsoWrongStateError() throws AuthenticationException {
        Mockito.when(mockSecretsHandler.getSecretsFromAws()).thenReturn(setUpTestSecretsMap());
        loginController.validateToken("", "", null, null);
        assertEquals("redirect:/login?ssoError", loginController.validateToken("", "",
            null, null));

    }

    @Test
    public void handleSsoAuthorisationError() throws AuthenticationException {
        Mockito.when(mockSecretsHandler.getSecretsFromAws()).thenReturn(setUpTestSecretsMap());
        loginController.validateToken("", "testState", "ERROR", "ERROR");

    }

    @Test(expected = HttpClientErrorException.class)
    public void handleSsoAccessTokenError() throws JSONException, AuthenticationException {

        JSONObject testResponseJSON = new JSONObject("{" +
            "\"error\": \"testError\"" +
            "}"
        );
        AwsSecretKeys awsSecretKeys = setUpTestSecretsMap();

        Mockito.when(mockSecretsHandler.getSecretsFromAws()).thenReturn(awsSecretKeys);
        Mockito.when(mockRestTemplate.postForEntity(anyString(), any(), any(Class.class)))
            .thenReturn(new ResponseEntity<>(testResponseJSON.toString(), HttpStatus.FORBIDDEN));
        loginController.validateToken("testToken", "testState", null, null);

    }

    private AwsSecretKeys setUpTestSecretsMap() {
        AwsSecretKeys awsSecretKeys = AwsSecretKeys.builder()
            .clientSecret("testSecret")
            .clientID("testClient")
            .signingAlgorithm("testAlgorithm")
            .state("testState")
            .tokenIssuer("testToken")
            .tenant("testTenant")
            .profileStorePassword("")
            .profileStoreUsername("")
            .build();

        return awsSecretKeys;
    }
}