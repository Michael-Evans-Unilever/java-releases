package com.gigya.cpp.profilestorewebapp.service;

import com.gigya.cpp.profilestorewebapp.model.mappings.BrandCodeMapping;
import com.gigya.cpp.profilestorewebapp.model.mappings.LanguageMapping;
import com.gigya.cpp.profilestorewebapp.model.mappings.MarketMapping;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/*
The tests in this class only test the methods abilities of getting the file and returning a list with the expected size
It does not test the ability of the mapping POJOs getters and setters to ensure the correct values are set.
*/

@RunWith(MockitoJUnitRunner.class)
public class CsvReaderServiceTest {
    private CsvReaderService csvReaderService;

    @Before
    public void init() {
        csvReaderService = new CsvReaderService();
    }

    @Test
    public void getAllBrands() throws FileNotFoundException {
        List<BrandCodeMapping> returnedList = csvReaderService.getAllBrands();
        assertEquals(9, returnedList.size());
    }

    @Test
    public void getAllLanguageTest() throws FileNotFoundException {
        List<LanguageMapping> returnedList = csvReaderService.getAllLanguage();
        assertEquals(5, returnedList.size());
    }

    @Test
    public void getAllMarketBrandTest() throws FileNotFoundException {
        Set<BrandCodeMapping> returnedList = csvReaderService.getAllMarketBrand("code1");
        assertEquals(2, returnedList.size());
    }

    @Test
    public void getAllMarkets() throws FileNotFoundException {
        List<MarketMapping> returnedList = csvReaderService.getAllMarkets();
        assertEquals(4, returnedList.size());
    }

    @Test
    public void getDataCenterMarketsTest() throws FileNotFoundException {
        String returnedDc1 = csvReaderService.getDataCenterMarkets("TestIso1");
        String returnedDc2 = csvReaderService.getDataCenterMarkets("TestIso2");
        assertEquals("TestDC1", returnedDc1);
        assertEquals("TestDC2", returnedDc2);
    }
}