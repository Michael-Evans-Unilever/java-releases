package com.gigya.cpp.profilestorewebapp.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigya.cpp.profilestorewebapp.config.CustomAuthentication;
import com.gigya.cpp.profilestorewebapp.config.OidcAuthentication;
import com.gigya.cpp.profilestorewebapp.config.UrlPropertyConfiguration;
import com.gigya.cpp.profilestorewebapp.model.pojos.AwsSecretKeys;
import com.gigya.cpp.profilestorewebapp.model.pojos.AzureKeys;
import java.util.ArrayList;
import java.util.List;
import lombok.SneakyThrows;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletWebRequest;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationServiceTest {

    private final static String TEST_URL = "https://TestSite/username/password";

    @Mock
    RestTemplate restTemplate;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private Authentication authentication;

    @Mock
    private SecretsHandler secretsHandler;

    @Mock
    private UrlPropertyConfiguration urlPropertyConfiguration;

    private AuthenticationService authenticationService;

    public AuthenticationServiceTest() throws JSONException {
    }

    private ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    private final String correctTestToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3RLZXlJRCJ9." +
        "eyJpc3MiOiJ0ZXN0SXNzdWVyIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMiwiZXhwIjo4MTE1" +
        "NTE3MTE4NzUxLCJlbWFpbCI6InRlc3RAZW1haWwuY29tIn0.jghOb4dQfzyhUnB8I1crAr7sSsScF8A0QYbfg0Mod7oSY2qdYx_2RkPa" +
        "SOkqg7EV8LpizaxDP7bgkzjsj_MNr5LOox-yVLCNYHSj7XEatJa6rzuI-7uDsVbW6W2X2r3Z9z2diueR2npmRRhKhn1lwQ1ksEZIdrK" +
        "ir84T1fQ8lAE";

    private final String incorrectAlgorithmTestToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3RLZXlJRCJ9." +
        "eyJpc3MiOiJ0ZXN0SXNzdWVyIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMiwiZXhwIjo4MTE" +
        "1NTE3MTE4NzUxLCJlbWFpbCI6InRlc3RAZW1haWwuY29tIn0.9Aaq3wHuy2nlOwJLolbtokAbo_7Y3anr92OgRS4UDno";

    private final String expiredDateTestToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3RLZXlJRCJ9.eyJpc3" +
        "MiOiJ0ZXN0SXNzdWVyIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMiwiZXhwIjoxMDAsImVtY" +
        "WlsIjoidGVzdEBlbWFpbC5jb20ifQ.DbFAfbdk22yx0M9loGJBtdnmGapM29s7dCQLhqrZzjJpxBg4ETIMHNkgp-kjtmQXHevewFMrM" +
        "ZxsoaAvahnR0Nm3xKedCshbu4SPB0_hz_2HArNU7zgKGV-W016vmkGB8it5C_lD30_RCQAXc68aeIlX8gpV3fpLVlF_5zmXD3Y";

    private final String incorrectIssuerTestToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3RLZXlJRCJ9.ey" +
        "Jpc3MiOiJpbmNvcnJlY3RJc3N1ZXIiLCJuYW1lIjoiSm9obiBEb2UiLCJhZG1pbiI6dHJ1ZSwiaWF0IjoxNTE2MjM5MDIyLCJleHAiO" +
        "jgxMTU1MTcxMTg3NTEsImVtYWlsIjoidGVzdEBlbWFpbC5jb20ifQ.PX8gmi2ziJaw48Bc-y6R8DsKEhtF5RvMdb9_7lmJaPYPlDP7P" +
        "lUTm_fKLIzRipb1qt48yqz2N2dbUS5knfzXypkpJ8tbVQB8O93b7OisYYTWHck8DXaf_6263XefBL0_RKJPBM2CV6fgo2U1L5kyfY4f" +
        "Wpr6O77nd0LYD_BUifY";


    @Before
    @SneakyThrows
    public void init() {
        authenticationService = new AuthenticationService(restTemplate, secretsHandler, urlPropertyConfiguration, objectMapper);
        when(urlPropertyConfiguration.getAzureSsoHost()).thenReturn("http://test.com");
        RequestAttributes mockRequest = new ServletWebRequest(new MockHttpServletRequest("GET", "/test"));
        RequestContextHolder.setRequestAttributes(mockRequest);
    }

    @Test
    public void udhdAuthenticateSuccessTest() {
        CustomAuthentication customAuthentication = new CustomAuthentication();
        customAuthentication.setAuthenticated(true);
        customAuthentication.setUsername("test@test.com");
        assertTrue(authenticationService.authenticate(customAuthentication) instanceof CustomAuthentication);
    }

    @Test(expected = BadCredentialsException.class)
    public void udhdAuthenticateFailTest() {
        CustomAuthentication customAuthentication = new CustomAuthentication();
        authenticationService.authenticate(customAuthentication);
    }


    @Test
    public void oidcAuthenticateSuccessTest() throws JSONException {

        AzureKeys testResponseJson = getAzureKeys("{" +
            "\"keys\":[" +
            "{" +
            "\"kty\":\"RSA\"," +
            "\"use\":\"sig\"," +
            "\"kid\":\"testKeyID\"," +
            "\"x5t\":\"testx5t\"," +
            "\"n\":\"n\"," +
            "\"e\":\"e\"," +
            "\"x5c\":[\"testx5c\"]}]" +
            "}");

        OidcAuthentication oidcAuthentication = new OidcAuthentication();
        oidcAuthentication.setAccessToken(correctTestToken);
        Mockito.when(secretsHandler.getSecretsFromAws()).thenReturn(setUpTestSecretsMap());
        Mockito.when(restTemplate.getForEntity(anyString(), any(Class.class)))
            .thenReturn(new ResponseEntity(testResponseJson, HttpStatus.OK));
        oidcAuthentication = (OidcAuthentication) authenticationService.authenticate(oidcAuthentication);
        assertEquals("ROLE_USER", oidcAuthentication.getAuthorities().get(0).toString());
    }

    @Test
    @Ignore
    public void oidcAdminAuthenticateSuccessTest() throws JSONException {

        AzureKeys testResponseJson = getAzureKeys("{" +
            "\"keys\":[" +
            "{" +
            "\"kty\":\"RSA\"," +
            "\"use\":\"sig\"," +
            "\"kid\":\"testKeyID\"," +
            "\"x5t\":\"testx5t\"," +
            "\"n\":\"n\"," +
            "\"e\":\"e\"," +
            "\"x5c\":[\"testx5c\"]}]" +
            "}");

        OidcAuthentication oidcAuthentication = new OidcAuthentication();
        oidcAuthentication.setAccessToken(correctTestToken);
        Mockito.when(secretsHandler.getSecretsFromAws()).thenReturn(setUpTestSecretsMap());
        Mockito.when(restTemplate.getForEntity(anyString(), any(Class.class)))
            .thenReturn(new ResponseEntity(testResponseJson, HttpStatus.OK));
        oidcAuthentication = (OidcAuthentication) authenticationService.authenticate(oidcAuthentication);
        assertEquals("ROLE_USER", oidcAuthentication.getAuthorities().get(0).toString());
        assertEquals("ROLE_ADMIN", oidcAuthentication.getAuthorities().get(1).toString());
    }

    @Test(expected = BadCredentialsException.class)
    public void oidcAuthenticationFailureWrongKeyID() throws JSONException {
        AzureKeys testResponseJson = getAzureKeys("{" +
            "\"keys\":[" +
            "{" +
            "\"kty\":\"RSA\"," +
            "\"use\":\"sig\"," +
            "\"kid\":\"failedTestKeyID\"," +
            "\"x5t\":\"testx5t\"," +
            "\"n\":\"n\"," +
            "\"e\":\"e\"," +
            "\"x5c\":[\"testx5c\"]}]" +
            "}");

        OidcAuthentication oidcAuthentication = new OidcAuthentication();
        oidcAuthentication.setAccessToken(correctTestToken);
        Mockito.when(secretsHandler.getSecretsFromAws()).thenReturn(setUpTestSecretsMap());
        Mockito.when(restTemplate.getForEntity(anyString(), any(Class.class))).thenReturn(new ResponseEntity(testResponseJson, HttpStatus.OK));
        authenticationService.authenticate(oidcAuthentication);
    }

    @Test(expected = BadCredentialsException.class)
    public void oidcAuthenticationFailureWrongAlgorithm() throws JSONException {
        AzureKeys testResponseJson = getAzureKeys("{" +
            "\"keys\":[" +
            "{" +
            "\"kty\":\"RSA\"," +
            "\"use\":\"sig\"," +
            "\"kid\":\"testKeyID\"," +
            "\"x5t\":\"testx5t\"," +
            "\"n\":\"n\"," +
            "\"e\":\"e\"," +
            "\"x5c\":[\"testx5c\"]}]" +
            "}");
        OidcAuthentication oidcAuthentication = new OidcAuthentication();
        oidcAuthentication.setAccessToken(incorrectAlgorithmTestToken);
        Mockito.when(secretsHandler.getSecretsFromAws()).thenReturn(setUpTestSecretsMap());
        Mockito.when(restTemplate.getForEntity(anyString(), any(Class.class)))
            .thenReturn(new ResponseEntity(testResponseJson, HttpStatus.OK));
        authenticationService.authenticate(oidcAuthentication);

    }

    @Test(expected = BadCredentialsException.class)
    public void oidcAuthenticationFailureExpiredDate() {
        AzureKeys testResponseJson = getAzureKeys("{" +
            "\"keys\":[" +
            "{" +
            "\"kty\":\"RSA\"," +
            "\"use\":\"sig\"," +
            "\"kid\":\"testKeyID\"," +
            "\"x5t\":\"testx5t\"," +
            "\"n\":\"n\"," +
            "\"e\":\"e\"," +
            "\"x5c\":[\"testx5c\"]}]" +
            "}");
        OidcAuthentication oidcAuthentication = new OidcAuthentication();
        oidcAuthentication.setAccessToken(expiredDateTestToken);
        Mockito.when(secretsHandler.getSecretsFromAws()).thenReturn(setUpTestSecretsMap());
        Mockito.when(restTemplate.getForEntity(anyString(), any(Class.class)))
            .thenReturn(new ResponseEntity(testResponseJson, HttpStatus.OK));
        authenticationService.authenticate(oidcAuthentication);
    }

    @Test(expected = BadCredentialsException.class)
    public void oidcAuthenticationFailureWrongTokenIssuer() {
        AzureKeys testResponseJson = getAzureKeys("{" +
            "\"keys\":[" +
            "{" +
            "\"kty\":\"RSA\"," +
            "\"use\":\"sig\"," +
            "\"kid\":\"testKeyID\"," +
            "\"x5t\":\"testx5t\"," +
            "\"n\":\"n\"," +
            "\"e\":\"e\"," +
            "\"x5c\":[\"testx5c\"]}]" +
            "}");
        OidcAuthentication oidcAuthentication = new OidcAuthentication();
        oidcAuthentication.setAccessToken(incorrectIssuerTestToken);
        Mockito.when(secretsHandler.getSecretsFromAws()).thenReturn(setUpTestSecretsMap());
        Mockito.when(restTemplate.getForEntity(anyString(), any(Class.class)))
            .thenReturn(new ResponseEntity(testResponseJson, HttpStatus.OK));
        authenticationService.authenticate(oidcAuthentication);
    }

    private AwsSecretKeys setUpTestSecretsMap() {
        AwsSecretKeys awsSecretKeys = AwsSecretKeys.builder()
            .clientSecret("testSecret")
            .clientID("testClient")
            .signingAlgorithm("RS256")
            .state("testState")
            .tokenIssuer("testIssuer")
            .tenant("testTenant")
            .clientSecret("testSecret")
            .profileStorePassword("")
            .profileStoreUsername("")
            .build();

        return awsSecretKeys;
    }

    @SneakyThrows
    private AzureKeys getAzureKeys(String json) {
        AzureKeys azureKeys = objectMapper.readValue(json, AzureKeys.class);

        return azureKeys;
    }
}
