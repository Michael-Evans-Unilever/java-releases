package com.gigya.cpp.profilestorewebapp.service.orchestrators;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import com.gigya.cpp.profilestorewebapp.config.KeysPropertyConfiguration;
import com.gigya.cpp.profilestorewebapp.config.UrlPropertyConfiguration;
import com.gigya.cpp.profilestorewebapp.model.pojos.FormJourney;
import com.gigya.cpp.profilestorewebapp.util.CppRestTemplate;
import com.gigya.cpp.profilestorewebapp.util.exceptions.StatusCodeNot2xxException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletWebRequest;

@RunWith(MockitoJUnitRunner.class)
public class FormJourneyOrchestratorTest {

    private final static String SANDBOX_API_KEY = "testKey";
    private final static String SET_SCREENSET_URL = "testUrl";

    private FormJourneyOrchestrator formJourneyOrchestrator;
    private FormJourney formJourneyDTO;

    @Mock
    private CppRestTemplate restTemplate;

    @Mock
    private UrlPropertyConfiguration urls;

    @Mock
    private KeysPropertyConfiguration keys;

    @Before
    public void init() {
        formJourneyDTO = Mockito.mock(FormJourney.class);
        formJourneyOrchestrator = new FormJourneyOrchestrator(restTemplate, urls, keys);
        when(keys.getKey("sandboxApiKey")).thenReturn(SANDBOX_API_KEY);
        when(urls.getSetFormJourney()).thenReturn(SET_SCREENSET_URL);
        RequestAttributes mockRequest = new ServletWebRequest(new MockHttpServletRequest("GET", "/test"));
        RequestContextHolder.setRequestAttributes(mockRequest);
    }

    @Test
    public void addScreenSetSuccessTest() {
        when(restTemplate.postRequest(any(HttpEntity.class), anyString())).thenReturn(ResponseEntity.ok(""));
        assertEquals(ResponseEntity.ok("").getStatusCode(), formJourneyOrchestrator.addFormJourney(formJourneyDTO));
    }

    @Test(expected = StatusCodeNot2xxException.class)
    public void addScreenSetFailureTest() {
        when(restTemplate.postRequest(any(HttpEntity.class), anyString())).thenThrow(new StatusCodeNot2xxException(HttpStatus.GATEWAY_TIMEOUT, ""));
        formJourneyOrchestrator.addFormJourney(formJourneyDTO);
    }
}