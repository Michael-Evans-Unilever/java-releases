package com.gigya.cpp.profilestorewebapp.util;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

import com.gigya.cpp.profilestorewebapp.config.KeysPropertyConfiguration;
import com.gigya.cpp.profilestorewebapp.config.UrlPropertyConfiguration;
import com.gigya.cpp.profilestorewebapp.service.SecretsHandler;
import com.gigya.cpp.profilestorewebapp.util.exceptions.StatusCodeNot2xxException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@RunWith(MockitoJUnitRunner.class)
public class CppRestTemplateTest {

    private static final HttpClientErrorException EXAMPLE_EXCEPTION =
        new HttpClientErrorException(HttpStatus.GATEWAY_TIMEOUT, "");
    private static final HttpEntity<String> TEST_ENTITY = new HttpEntity<>("{}");
    private static final String TEST_URL = "test.url";

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private SecretsHandler secretsHandler;

    @Mock
    private UrlPropertyConfiguration urls;

    @Mock
    private KeysPropertyConfiguration keys;

    private CppRestTemplate cppRestTemplate;

    @Before
    public void setup() {
        cppRestTemplate = new CppRestTemplate(restTemplate, secretsHandler, urls, keys);
    }

    @Test
    public void postRequestTest() {
        cppRestTemplate.postRequest(TEST_ENTITY, TEST_URL);
        Mockito.verify(restTemplate,
            Mockito.times(1))
            .postForEntity(TEST_URL, TEST_ENTITY, String.class);
    }

    @Test(expected = StatusCodeNot2xxException.class)
    public void postRequestWithException() {
        Mockito.when(restTemplate.postForEntity(anyString(), any(HttpEntity.class), any()))
            .thenThrow(EXAMPLE_EXCEPTION);
        cppRestTemplate.postRequest(TEST_ENTITY, TEST_URL);

    }

    @Test
    public void getRequest() {
        cppRestTemplate.getRequest(TEST_ENTITY, TEST_URL);
        Mockito.verify(restTemplate).exchange(TEST_URL, HttpMethod.GET, TEST_ENTITY, String.class);
    }

    @Test(expected = StatusCodeNot2xxException.class)
    public void getRequestWithException() {
        Mockito.when(restTemplate.exchange(TEST_URL, HttpMethod.GET, TEST_ENTITY, String.class))
            .thenThrow(EXAMPLE_EXCEPTION);
        cppRestTemplate.getRequest(TEST_ENTITY, TEST_URL);
    }
}