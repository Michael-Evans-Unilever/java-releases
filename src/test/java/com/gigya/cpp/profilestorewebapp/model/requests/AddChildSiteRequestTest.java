package com.gigya.cpp.profilestorewebapp.model.requests;


import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AddChildSiteRequestTest {

    @Test
    public void AddChildSiteRequestGettersAndSettersTest() {

        AddChildSiteRequest addChildSiteRequest = new AddChildSiteRequest();
        addChildSiteRequest.setApiKey("testApiKey");
        addChildSiteRequest.setBrand("testBrand");
        addChildSiteRequest.setCampaignId("testCampaignId");
        addChildSiteRequest.setEmailServiceId("testEmailServiceId");
        addChildSiteRequest.setNewsletterServiceId("testNewsletterServiceId");
        addChildSiteRequest.setPostServiceId("testPostServiceId");
        addChildSiteRequest.setUrl("testUrl");
        addChildSiteRequest.setDoubleOptIn("testDoubleOptIn");
        addChildSiteRequest.setTerms("testTerms");
        addChildSiteRequest.setPrivacy("testPrivacy");
        addChildSiteRequest.setCookie("testCookie");
        assertEquals("testApiKey", addChildSiteRequest.getApiKey());
        assertEquals("testBrand", addChildSiteRequest.getBrand());
        assertEquals("testCampaignId", addChildSiteRequest.getCampaignId());
        assertEquals("testEmailServiceId", addChildSiteRequest.getEmailServiceId());
        assertEquals("testNewsletterServiceId", addChildSiteRequest.getNewsletterServiceId());
        assertEquals("testPostServiceId", addChildSiteRequest.getPostServiceId());
        assertEquals("testUrl", addChildSiteRequest.getUrl());
        assertEquals("testDoubleOptIn", addChildSiteRequest.getDoubleOptIn());
        assertEquals("testTerms", addChildSiteRequest.getTerms());
        assertEquals("testPrivacy", addChildSiteRequest.getPrivacy());
        assertEquals("testCookie", addChildSiteRequest.getCookie());

    }
}