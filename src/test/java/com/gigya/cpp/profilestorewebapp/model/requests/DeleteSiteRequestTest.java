package com.gigya.cpp.profilestorewebapp.model.requests;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DeleteSiteRequestTest {

    @Test
    public void DeleteSiteRequestGettersAndSettersTest() {

        DeleteSiteRequest deleteSiteRequest = new DeleteSiteRequest();
        deleteSiteRequest.setPartnerID("testPartnerId");
        assertEquals("testPartnerId", deleteSiteRequest.getPartnerID());

    }

}