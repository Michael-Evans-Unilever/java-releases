package com.gigya.cpp.profilestorewebapp.model.requests;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.Test;

public class AddFormJourneyRequestTest {

    String payLoad="{\n"
        + "  \"formJourney\": \"abcd\",\n"
        + "  \"siteApiKey\": \"3_mQq97UCXTZo9IseeDQ9n5oRo_F1Qd2da-s07nK9FJ949UEMNYCKBxJC5o1c1jb9J\",\n"
        + "  \"market\": \"ar\",\n"
        + "  \"language\": [\n"
        + "    \"ar\",\n"
        + "    \"bs\",\n"
        + "    \"en\"\n"
        + "  ],\n"
        + "  \"templateselector\": \"qandatemplate\",\n"
        + "  \"terms\": \"https:\\/\\/www.xyz.com\",\n"
        + "  \"campaignId\": \"\",\n"
        + "  \"_csrf\": \"4c5f3391-9f6b-4b77-8ab2-2b47398a7d54\",\n"
        + "  \"brandCode\": \"bh1082\",\n"
        + "  \"dataCentre\": \"us1\",\n"
        + "  \"qanda\": [\n"
        + "    {\n"
        + "      \"categories\": \"Cooking\",\n"
        + "      \"qaquestion\": \"2880\",\n"
        + "      \"answer\": [\n"
        + "        \"7540\",\n"
        + "        \"5909\"\n"
        + "      ],\n"
        + "      \"questionText\": \"Do you cook daily?\"\n"
        + "    }\n"
        + "  ]\n"
        + "}";

    @SneakyThrows
    @Test
    public void testObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.readValue(payLoad, AddFormJourneyRequest.class);
    }

}