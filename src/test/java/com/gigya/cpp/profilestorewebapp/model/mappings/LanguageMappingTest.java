package com.gigya.cpp.profilestorewebapp.model.mappings;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LanguageMappingTest {

    @Test
    public void LanguageMappingGettersAndSettersTest() {
        LanguageMapping languageMapping = new LanguageMapping("testEnglishName","testIsoCode");
        assertEquals("testEnglishName", languageMapping.getEnglishName());
        assertEquals("testIsoCode", languageMapping.getIsoCode());
    }

}