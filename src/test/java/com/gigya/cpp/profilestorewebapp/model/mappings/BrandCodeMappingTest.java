package com.gigya.cpp.profilestorewebapp.model.mappings;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BrandCodeMappingTest {

    @Test
    public void BrandCodeMappingGettersAndSettersTest() {

        BrandCodeMapping brandCodeMapping = new BrandCodeMapping();
        brandCodeMapping.setBrandName("testBrandName");
        brandCodeMapping.setBrandCode("testBrandCode");
        assertEquals("testBrandName", brandCodeMapping.getBrandName());
        assertEquals("testBrandCode", brandCodeMapping.getBrandCode());

    }

}