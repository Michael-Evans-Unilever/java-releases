package com.gigya.cpp.profilestorewebapp.model.pojos;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.stream.IntStream;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
class QuestionTest {
  private static final Faker FAKER = new Faker();
  private static final Random RANDOM = new Random();

  @Test
  void setQuestionTextList() throws JsonProcessingException {

    QuestionOa3 questionOa3 = new QuestionOa3();
    questionOa3.setId(RANDOM.nextLong());
    questionOa3.setCategory("Cooking");
    questionOa3.setCategoryCode("QC0008");
    questionOa3.setSensitivity("highly");
    questionOa3.setSensitivityCode("HS1");

    String[] isoCountries = Locale.getISOCountries();
    List<TextI18n> texts = new ArrayList<>();

    IntStream.range(0, 3).forEach(i -> {
        TextI18n i18n1 = new TextI18n();
        i18n1.setParentId(questionOa3.getId());
        int randomInt = RANDOM.nextInt(isoCountries.length);
        i18n1.setIso2CountryCode(isoCountries[randomInt]);
        Locale iso3 = new Locale("", i18n1.getIso2CountryCode());
        i18n1.setIso3CountryCode(iso3.getISO3Country());
        i18n1.setText(FAKER.gameOfThrones().quote());
        texts.add(i18n1);
    });

    questionOa3.setQuestionTextList(texts);

    ObjectMapper mapper = new ObjectMapper();
    String jsonInString = mapper.writeValueAsString(questionOa3);
    log.debug("JSon: {}", jsonInString);
  }

  @Test
  void setAnswerTextList() throws JsonProcessingException {

    AnswerOa3 answer = new AnswerOa3();
    answer.setId(RANDOM.nextLong());
    answer.setCategory("Cooking");
    answer.setCategoryCode("QC0008");

    String[] isoCountries = Locale.getISOCountries();
    List<TextI18n> texts = new ArrayList<>();

    IntStream.range(0, 3).forEach(i -> {
      TextI18n i18n1 = new TextI18n();
      i18n1.setParentId(answer.getId());
      int randomInt = RANDOM.nextInt(isoCountries.length);
      i18n1.setIso2CountryCode(isoCountries[randomInt]);
      Locale iso3 = new Locale("", i18n1.getIso2CountryCode());
      i18n1.setIso3CountryCode(iso3.getISO3Country());
      i18n1.setText(FAKER.gameOfThrones().quote());
      texts.add(i18n1);
    });

    answer.setAnswerTextList(texts);

    ObjectMapper mapper = new ObjectMapper();
    String jsonInString = mapper.writeValueAsString(answer);
    log.debug("JSon: {}", jsonInString);
  }
}