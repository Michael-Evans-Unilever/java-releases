package com.gigya.cpp.profilestorewebapp.model.mappings;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MarketMappingTest {

    @Test
    public void LanguageMappingGettersAndSettersTest() {

        MarketMapping marketMapping = new MarketMapping();
        marketMapping.setMarket("testMarket");
        marketMapping.setCountryCode("testCountryCode");
        assertEquals("testMarket", marketMapping.getMarket());
        assertEquals("testCountryCode", marketMapping.getCountryCode());

    }

}