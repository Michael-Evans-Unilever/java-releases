package com.gigya.cpp.profilestorewebapp;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ComponentScan(basePackages = {"com.gigya.cpp.profilestorewebapp"})
@SpringBootTest(classes = {ProfileStoreWebappApplication.class})
public class ContextLoadsTest {

    @Test
    public void contextLoads() {
    }
}